# SAMSUNG_MULTICAMPUS

2020.12.11~2021.02.04🤖🤖
자바 알고리즘을 이용한 반응형 웹 프로그래밍 전문가 과정

### **학습목표**
***
- 관계형 데이터베이스에서 SQL을 사용하여 목적에 적합한 데이터를 정의하고 원하는 결과를 도출할 수 있다.
- 응용소프트웨어 개발에 사용되는 프로그래밍 언어의 기초문법을 적용하고 언어의 특징과 라이브러리를 활용하여 기본 응용소프트웨어를 구현할 수 있다.
- 사용자의 요구사항을 파악하고 분석하여 인터페이스, 서버프로그램을 구현할 수 있다.
- 배열, 탐욕, 수학 응용 등 기본적인 알고리즘 유형을 학습하고 실전문제해결을 위한 일련의 순서화된 절차를 구성할 수 있다.


<img src="/uploads/e6c4a625b6141e997ee2467e97438ad3/img_05.jpg">
