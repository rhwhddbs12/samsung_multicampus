package service;

import java.util.List;

import vo.VO;

public interface DeptService {
	
	
	public int insertDept(VO vo);
	public void updateDept(VO vo);
	public void deleteDept(int deptno);
	public  VO getDept(int deptno);
	public  List<VO> getDeptAll();
	
}
