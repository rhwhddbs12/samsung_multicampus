package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import Util.JDBCUtil;
import vo.VO;

/**
 * data access object
 * Dept table CRUD 작업 JDBC코
 *
 */


public class DeptDAO {
	public int insertDept(VO vo) {  //파라미터 세개 값이 들어가는것보다 이렇게 객체하나로 많이씀
		Connection con = null;
		Statement st = null; // SQL구문을 처리
		PreparedStatement ps = null;
		ResultSet rs = null; // Select타입의 결과 
		int row = 0; 
		
		String sql = "insert into dept(deptno,dname,loc) values(?,?,?)";

		try {
			con = JDBCUtil.getConnection();
			ps = con.prepareStatement(sql);
			//? 세팅작업
			ps.setInt(1,vo.getDeptno());
			ps.setString(2,vo.getDname());
			ps.setString(3,vo.getLoc());
			
			row = ps.executeUpdate(); //insert, delete, update
			//결과 값 핸들
			
		}catch(Exception e){
			e.printStackTrace();
		}finally {
			JDBCUtil.close(con, ps, rs);
		}
		return row;
	}
	
	
		public void updateDept(VO vo) {
		
		Connection con = null;
		PreparedStatement ps = null;
		
		ResultSet rs = null; // Select타입의 결과 
		int row = 0; 
		
		String sql = "Update dept set dname =,loc = where deptno= ";

		try {
			
			con = JDBCUtil.getConnection();
			ps = con.prepareStatement(sql);
			//? 세팅작업
			
			ps.setInt(1, vo.getDeptno());
			ps.setString(2, vo.getDname());
			ps.setString(3, vo.getLoc());
			
			row = ps.executeUpdate(); //insert, delete, update
			//결과 값 핸들
			
		}catch(Exception e){
			e.printStackTrace();
		}finally {
			JDBCUtil.close(con, ps, rs);
		}
	}
		
	public void deleteDept(int deptno) {
		
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null; // Select타입의 결과 
		
		int row = 0; 
		
		String sql = "delete from dept where deptno = 50";

		try {
			con = JDBCUtil.getConnection();
			ps = con.prepareStatement(sql);
			//? 세팅작업
			ps.setInt(1, deptno);
			row = ps.executeUpdate(); //insert, delete, update
			//결과 값 핸들
			
		}catch(Exception e){
			e.printStackTrace();
		}finally {
			JDBCUtil.close(con, ps, rs);
		}
		
	}
		
	public  VO getDept(int deptno) {
    	Connection con = null;
		PreparedStatement ps = null;
		
		ResultSet rs = null;
		
		String sql = "select * from dept where deptno = ?";
		VO vo = null;
		
		try {
			con = JDBCUtil.getConnection();
			ps = con.prepareStatement(sql);
			// ? 세팅작업
			ps.setInt(1, deptno);
			
			rs = ps.executeQuery();   // select
			//결과값 핸들링
			while(rs.next()) {
				vo = new VO();
				vo.setDeptno( rs.getInt("DEPTNO"));
				vo.setDname(rs.getString("DNAME"));
				vo.setLoc(rs.getString("LOC"));
			}
			
		}catch (Exception e) {
			e.printStackTrace();
		}finally {
			JDBCUtil.close(con, ps, rs);
		}
		
		return vo;
	}
	public  List<VO> getDeptAll() {
    	Connection con = null;
		PreparedStatement ps = null;
		
		ResultSet rs = null;
		int row = 0;
		
		String sql = "select * from dept order by deptno";
		List<VO> list = new ArrayList<VO>();
		
		try {
			con = JDBCUtil.getConnection();
			ps = con.prepareStatement(sql);
			// ? 세팅작업
			
			rs = ps.executeQuery();   // select
			//결과값 핸들링
			while(rs.next()) {
				VO vo = new VO();
				vo.setDeptno( rs.getInt("DEPTNO"));
				vo.setDname(rs.getString("DNAME"));
				vo.setLoc(rs.getString("LOC"));
				list.add(vo);
			}
			
		}catch (Exception e) {
			e.printStackTrace();
		}finally {
			JDBCUtil.close(con, ps, rs);
		}
		
		return list;
	}
}

