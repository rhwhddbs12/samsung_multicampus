package Test;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Test01 {

	public static void main(String[] args) {
		
		//oracle
		String driver = "oracle.jdbc.driver.OracleDriver";
		String url ="jdbc:oracle:thin:@127.0.0.1:55003:xe";
		String user = "system";
		String pw = "1234";
		
		String sql = "select * from emp";
		
		Connection con = null;
		Statement st = null;
		ResultSet rs = null;
		int result = 0;
		
		try {
			//1. Driver class로딩
			Class.forName(driver);
			//2. 로딩된 Driver클래스를 이용해 connection요청(url,user,pwd)
			con = DriverManager.getConnection(url,user,pw);
			//3. 생성된 connection으로부터 Statement생성
			st = con.createStatement();
			//4. 생성된 Statement를 이용해서 sql수행(execute,executeUpdate,executeQuery)
			rs = st.executeQuery(sql);
			//5. 결과처리(ResultSet,int)
			while(rs.next()) {
				System.out.print(rs.getString("ENAME")+"\t");
				System.out.print(rs.getString("JOB")+"\t");
				System.out.print(rs.getDate("HIREDATE")+"\t");
				System.out.print(rs.getInt("SAL")+"\t");
				System.out.print(rs.getString("DEPTNO")+"\n");
			}
			
		}catch(Exception e) {
			System.out.println(e.getMessage());
		}finally {
			//7. 자원정리(connection, statement, resultset)
			try {
				if(rs != null) rs.close();
				if(st != null) st.close();
				if(con != null) con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			
		}
	}

}
