package Test;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import Util.JDBCUtil;
import vo.VO;

public class Prob2 {
	  

		public static void main(String[] args) {
		Prob2 p = new Prob2(); //show()메서드가 static메서드가 아니라 인스턴스메서드이기 때문에 객체생성해야함
		p.show(10);
		System.out.println("============================");
		p.show(20);
		System.out.println("============================");
		
		VO d = new VO();
		VO vo_50 = p.getDept(50);
		VO vo_20 = p.getDept(20);
		System.out.println(vo_20);
		
		p.getDeptAll().forEach(i->System.out.println(i));;
	}	


	
    private void show(int DEPARTMENT_ID) {
    	
		String sql = "select avg(sal) \"평균급여\" \n" + 
				"from emp E, departments D\n" + 
				"where E.deptno = D.department_id\n" + 
				"and E.deptno = ? ";  
		// ? 처리함으로서 오라클입장에서 성능이좋아지고 자바입장에서 보안상 좋아졌다.
		
		Connection con = null;
//		Statement st = null;  //조건절이 없는 셀렉트문을 쓸떈 써도좋다
		PreparedStatement ps = null;// 조건문이 있는 셀렉트문에서 보안,성능적인 측면에서 좋아 거의 이거쓴다.
		ResultSet rs = null;
		int result = 0;
		
		try {
			
			con = JDBCUtil.getConnection();
			//3. 생성된 connection으로부터 Statement생성
			ps = con.prepareStatement(sql); //미리 SQL문을 컴파일한다
			ps.setInt(1, DEPARTMENT_ID); // ? 값을 넣어준다.
			//**********자바는 0부터 인덱스 오라클은 1부터 인덱스 그래서 0이아니라 1이들어갔음 ******************
			
			//4. 생성된 Statement를 이용해서 sql수행(execute,executeUpdate,executeQuery)
			rs = ps.executeQuery(); //
			//5. 결과처리(ResultSet,int)
			while(rs.next()) {
				System.out.printf("부서 %d의 평균급여:"+rs.getInt("평균급여")+"\n",DEPARTMENT_ID);
			}
			
		}catch(Exception e) {
			System.out.println(e.getMessage());
		}finally {
			JDBCUtil.close(con, ps, rs);
		}
    }

    public  VO getDept(int DEPARTMENT_ID) {
    	Connection con = null;
		PreparedStatement ps = null;
		
		ResultSet rs = null;
		int row = 0;
		
		String sql = "select * from dept where deptno = ?";
		VO vo = null;
		
		try {
			con = JDBCUtil.getConnection();
			ps = con.prepareStatement(sql);
			// ? 세팅작업
			ps.setInt(1, DEPARTMENT_ID);
			
			rs = ps.executeQuery();   // select
			//결과값 핸들링
			while(rs.next()) {
				vo = new VO();
				vo.setDeptno( rs.getInt("DEPTNO"));
				vo.setDname(rs.getString("DNAME"));
				vo.setLoc(rs.getString("LOC"));
			}
			
		}catch (Exception e) {
			e.printStackTrace();
		}finally {
			JDBCUtil.close(con, ps, rs);
		}
		
		return vo;
	}
    public List<VO> getDeptAll() {
    	

		Connection con = null;
//		Statement st = null; // SQL구문을 처리 보안상 위험하다 SQL injection 발생할 수 있음
		PreparedStatement ps = null;
		
		ResultSet rs = null; // Select타입의 결과 
		int row = 0; 
		
		List<VO> list = new ArrayList<VO>();
		String sql = "select * from dept order by deptno ";

		try {
			con = JDBCUtil.getConnection();
			ps = con.prepareStatement(sql);
			//? 세팅작업
			
			rs = ps.executeQuery();   // select
			
			//결과 값 핸들
			while(rs.next()) {
				
				VO vo = new VO();
				vo.setDeptno(rs.getInt("deptno"));
				vo.setLoc(rs.getString("loc"));
				vo.setDname(rs.getString("dname"));
				list.add(vo);
//				System.out.print(rs.getString("DEPTNO")+"\t");
//				System.out.print(rs.getString("DNAME")+"\t");
//				System.out.print(rs.getString("LOC")+"\t");
			}
		}catch(Exception e){
			e.printStackTrace();
		}finally {
			JDBCUtil.close(con, ps, rs);
		}
		return list;
    }
}
    
    