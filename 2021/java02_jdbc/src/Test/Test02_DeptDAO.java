package Test;

import Util.JDBCUtil;
import dao.DeptDAO;
import vo.VO;

public class Test02_DeptDAO {

	public static void main(String[] args) {
		DeptDAO dao = new DeptDAO();
		
//		int row = dao.insertDept(50,"교육부","서울시");
		VO vo = new VO();
		vo.setDeptno(50);
		vo.setDname("영업부");
		vo.setLoc("제주도");
		
		int row = dao.insertDept(vo);
	
		VO data = dao.getDept(50);
		if(data != null) {
			data.setDname("~~~");
			data.setLoc("SEOUL");
			dao.updateDept(data);
		}
//		dao.deleteDept(50);
	}
}