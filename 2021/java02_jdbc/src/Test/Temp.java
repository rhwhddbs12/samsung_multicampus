package Test;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

import Util.JDBCUtil;

public class Temp {

	public static void main(String[] args) {
		
		Connection con = null;
		Statement st = null; // SQL구문을 처리
		PreparedStatement ps = null;
		ResultSet rs = null; // Select타입의 결과 
		int row = 0; 
		
		String sql = "";

		try {
			con = JDBCUtil.getConnection();
			ps = con.prepareStatement(sql);
			//? 세팅작업
			rs = ps.executeQuery();   // select
			row = ps.executeUpdate(); //insert, delete, update
			//결과 값 핸들
			
		}catch(Exception e){
			e.printStackTrace();
		}finally {
			JDBCUtil.close(con, ps, rs);
		}
	}

}
