package Test;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import Util.JDBCUtil;

public class Prob {
	  
	
    private static void show(int DEPARTMENT_ID) {
    	
		String sql = "select avg(sal) \"평균급여\" \n" + 
				"from emp E, departments D\n" + 
				"where E.deptno = D.department_id\n" + 
				"and E.deptno =" +DEPARTMENT_ID;
		
		Connection con = null;
		Statement st = null;
		ResultSet rs = null;
		int result = 0;
		
		try {
			
			con = JDBCUtil.getConnection();
			//3. 생성된 connection으로부터 Statement생성
			st = con.createStatement();
			//4. 생성된 Statement를 이용해서 sql수행(execute,executeUpdate,executeQuery)
			rs = st.executeQuery(sql);
			//5. 결과처리(ResultSet,int)
			while(rs.next()) {
				System.out.printf("부서 %d의 평균급여:"+rs.getInt("평균급여")+"\n",DEPARTMENT_ID);
			}
			
		}catch(Exception e) {
			System.out.println(e.getMessage());
		}finally {
			JDBCUtil.close(con, st, rs);
		}
    }


public static void main(String[] args) {
	show(10);
	System.out.println("============================");
	show(50);
	System.out.println("============================");
	}	
}
