--job이 매니저이고 급여는 1500원이상이며 리서치에서 근무하는 사워
select * from emp;

select empno 사원번호, ename 사원 , e.deptno 부서번호, dname 부서이름
from emp e, dept d
where e.deptno = d.deptno and job = 'MANAGER' and dname = 'RESEARCH' AND SAL >=1500;

--사원이 한명도 없는 부서정보
SELECT *
FROM DEPT
WHERE DEPTNO NOT IN ( SELECT DEPTNO FROM EMP );

--부서별 평균 급여가 2000 이상
SELECT D.DEPTNO,DNAME|| ROUND(AVG(SAL))
FROM EMP E, DEPT D
WHERE E.DEPTNO = D.DEPTNO
GROUP BY DNAME, D.DEPTNO
HAVING AVG(SAL) >= 2000;