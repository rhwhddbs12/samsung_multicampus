--@c:\lib\datatable.sql;

SELECT
    ename,
    job,
    to_char(hiredate,'yyyy "년"') as 입사일,
    sal,
    comm,
    deptno
FROM
    emp;
    
-- 구문처리순서
-- from -> where ->  select -> order by
    
select ename,sal*12 급여,deptno
from emp 
where  sal*12 >30000
order by 급여 desc;


select * from dept;
select * from emp;

select ename,sal,dept.deptno,dname,loc 
from emp
join dept
on emp.deptno=dept.deptno
where dept.deptno=10
order by 3,sal;

select ename,sal,deptno,dname,loc 
from emp  natural join dept;

--on emp.deptno=dept.deptno

select ename,sal,deptno,dname,loc 
from emp  join dept
using(deptno);

select ename,sal,dept.deptno,dname,loc 
from emp right outer join dept
on emp.deptno=dept.deptno;

select * from salgrade;

select ename,sal,grade  
from emp 
     join salgrade
     on sal BETWEEN losal and hisal;
     

select ename,sal,grade  ,dept.deptno,dname,loc 
from emp 
     join salgrade
     on sal BETWEEN losal and hisal
     join dept
     on emp.deptno = dept.deptno;


-- self join
select  e.ename 사원이름 , e.sal 사원급여 , nvl(m.ename,'<<CEO>>') 상사이름 , m.sal 상사급여
from  emp e left outer join emp m
on e.mgr = m.empno;

select  e.ename 사원이름 , e.sal 사원급여 ,m.ename 상사이름 , m.sal 상사급여
from  emp e left outer join emp m
on e.mgr = m.empno;

-- 상사보다 먼저 입사한 사원의 목록 
select  e.ename 사원이름 , e.hiredate 사원입사일 , nvl(m.ename,'<<CEO>>') 상사이름 , m.hiredate 입사일
from  emp e left outer join emp m
on e.mgr = m.empno
where e.hiredate < m.hiredate; -- where 대신 and


-- 6장  집계함수, 그룹함수
select ename , lower(ename) , hiredate, to_char(hiredate,'mm')
from emp;

SELECT round(avg(sal),2) as 평균급여
from emp;

SELECT lower(ename),avg(sal)  -- X
from emp;

SELECT sum(sal),count(sal),sum(sal)/count(sal)  ,avg(sal),max(sal),min(sal)
from emp;

select COUNT(DISTINCT job) from emp;


SELECT   round(avg(sal))  FROM emp;
SELECT   round(avg(sal))  FROM emp where deptno =10;
SELECT   round(avg(sal))  FROM emp where deptno =20;
SELECT   round(avg(sal))  FROM emp where deptno =30;
SELECT   round(avg(sal))  FROM emp where deptno =40;

select * from emp ORDER by deptno;


SELECT
from
where
group by
order by


-- 부서별 요약 분석 
SELECT deptno , round(avg(sal)) 평균급여,max(sal) 최고급여,min(sal) 최소급여
from  emp
group by deptno
order by deptno;

-- sal , deptno , dname 

SELECT emp.deptno , dname,round(avg(sal)) 평균급여,max(sal) 최고급여,min(sal) 최소급여
from  emp join dept
on emp.deptno = dept.deptno
group by emp.deptno , dname
order by emp.deptno;

-- loc 도시이름별 평균 급여를 구하세요. 
SELECT loc,round(avg(sal)) 평균급여
from  emp join dept
on emp.deptno = dept.deptno
group by loc
order by 2;

-- emp 테이블에서 월별 입사자수 
select * from emp order by to_char(hiredate,'mm');

SELECT to_char(hiredate,'mm') 월,count(*) 입사자수
from emp
group by to_char(hiredate,'mm')
order by 1;

SELECT to_char(hiredate,'day') 요일,count(*) 입사자수
from emp
group by to_char(hiredate,'day')
order by 1;

-- 부서이름별 평균 급여가 2000 이상인 리스트. 
SELECT dname,round(avg(sal)) 평균급여
from  emp join dept
on emp.deptno = dept.deptno
group by dname
having round(avg(sal)) > 2000
order by 2;

SELECT dname,round(avg(sal)) 평균급여
from  emp join dept
on emp.deptno = dept.deptno
where sal > 2000
group by dname
having round(avg(sal)) > 2000
order by 2;

-- rollup   cube

select deptno,job,sum(sal)
from emp
group by rollup(deptno,job)
order by 1,2;


-- 8장 SubQuery
SELECT * FROM emp;

-- FORD 사원 보다 급여를 많이 받는 사원 리스트 

SELECT sal FROM emp where ename='FORD';

SELECT * 
FROM emp
where sal >(SELECT sal FROM emp where ename='FORD');

-- 평균 급여보다 적게 받는 사원 리스트
SELECT * 
FROM emp
where sal <(SELECT avg(sal) FROM emp);

-- 급여가 제일 적은 사원 
SELECT min(sal) FROM emp;

SELECT * 
FROM emp
where sal = (SELECT min(sal) FROM emp);

-- 스칼라=단일값  ,   벡터=다중값
SELECT min(sal) FROM emp;
SELECT min(sal) FROM emp group by deptno;

-- 부서별 최저급여 리스트
SELECT deptno,ename,sal  
FROM emp
where sal in (SELECT min(sal) FROM emp group by deptno);

SELECT deptno,ename,sal  
FROM emp
where sal in (800,950,1300)

SELECT deptno,ename,sal  
FROM emp
where sal=800 or sal=950 or sal =1300;

-- 부서별 최고급여 리스트
SELECT deptno,ename,sal 
FROM emp
where (deptno,sal) in (SELECT deptno,max(sal) FROM emp group by deptno);

SELECT deptno,ename,sal 
FROM emp
where (deptno,sal) in ((30,2850),(20,3000),(10,5000));

SELECT deptno,ename,sal 
FROM emp
where (deptno=30 and sal =2850) or (deptno=20 and sal =3000)or (deptno=10 and sal =5000);

SELECT deptno,ename,sal 
FROM emp
where deptno not in (10,30);

SELECT deptno,ename,sal 
FROM emp
where deptno  < any (10,30);

SELECT deptno,ename,sal 
FROM emp
where deptno  < all (20,30);

-- 상관관계 쿼리문
-- 자신이 속한 부서의 평균 급여보다 적은 사원 리스트
SELECT * 
FROM emp e
where sal <(SELECT avg(sal) FROM emp where deptno=e.deptno);

SELECT avg(sal) FROM emp where deptno=20;

-- 1 => 1~5   2 page => 6 ~10
select * from 
( SELECT rownum row#,deptno , job,  ename,     hiredate 입사일 ,    sal as 급여
  from (SELECT * from emp order by sal desc))
where row# between 11 and 14;

select * from 
( SELECT rownum row#,deptno , job,  ename,     hiredate 입사일 ,    sal as 급여
  from (SELECT * from emp order by sal desc))
where row# between &start and &and;

-- mysql  ==>  SELECT * from emp order by sal desc limit 10,15


-- 9장  집합연산자

select job from emp where deptno = 10;
select job from emp where deptno = 20;


select job,'aa' from emp where deptno = 10 
union
select job,null from emp where deptno = 20 order by 1;


select job from emp where deptno = 10
union all
select job from emp where deptno = 20;


select job from emp where deptno = 10
intersect
select job from emp where deptno = 20;


select job from emp where deptno = 10
minus
select job from emp where deptno = 20;


