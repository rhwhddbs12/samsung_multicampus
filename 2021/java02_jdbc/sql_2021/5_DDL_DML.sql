-10장 데이터 조작어 DML : INSERT UPDATE DELETE =>rollback 가능
-11장 DDL TABLE 생성 (auto commit) => rollback 불가

--프로젝트 => crud : 삽입,수정,삭제,검색
--SQL : DML,DCL,DDL,SQL쿼리

DROP TABLE book;
create table book(
    bookno number(5),
    title varchar2(30), 
    author varchar2(30) ,
    pubdate date
);

desc book;

insert into book(bookno,title,author,pubdate)
values(3,'spring','','');
-- 커밋을 안하면 다른곳에서 디비를 봤을떄 데이터정보를 볼 수 없음.
commit; 
rollback;
select * from book;

insert into book(bookno,title)
values(3,'spring');

insert into book(bookno,title,author,pubdate)
values(3,'html5','고길동','2017/1/5');

--날짜포맷
insert into book(bookno,title,author,pubdate)
values(3,'html5','고길동',to_date('12/30/2017','mm/dd/yyyy'));

delete from book; -- rollback 가능
delete from book where bookno = 3; -- rollback 가능

--단일 업데이트
update book set pubdate=sysdate;
--여러개 업데이트
update book set pubdate='20/1/1' , title='...';

insert into book(bookno,title,author,pubdate)
values(5,'jQuery','이길동',to_date('12/30/2017','mm/dd/yyyy'));

--테이블구조 변경 (컬럼추가)
alter table book add(price number(7)); --auto commit
--테이블구조 변경 (컬럼삭제)
alter table book drop column price; --auto commit
--소수점
alter table book modify(price number(3,2)); --auto commit
--테이블 이름 변경
rename book2 to book;


select * from book;
update book set price =0 where price is null;
rollback;
insert into book(bookno,title,author,pubdate,price)
values(4,'html5','고길동',to_date('12/30/2017','mm/dd/yyyy'),900);
insert into book(bookno,title,author,pubdate,price)
values(7,'html5','고길동',to_date('12/30/2017','mm/dd/yyyy'),900.99);

DELETE from bookl -- rollback 가능
DROP TABLE book; -- auto commit
TRUNCATE TABLE book; --auto commit
##################################################
--기존테이블로 새로운 테이블 만들기
CREATE TABLE EMP2 AS SELECT * FROM EMP;
CREATE TABLE DEPT2 AS SELECT * FROM DEPT;

desc dept;

select *from dept2;

insert into dept2
values(50,'EDU','SEOUL');

commit;
select *from emp2;

insert into emp(empno,ename,hiredate,sal,deptno)
            values(9999,'홍길동',sysdate,0,90);

insert into emp2(empno,ename,hiredate,sal,deptno)
            values(9999,'홍길동',sysdate,0,90);            
            
-- drop table (auto commit)
drop table emp2;
-- drop된 휴지통에 있는 아이들 복구할때 사용
select*from recyclebin;
-- 복구
flashback table emp2 to before drop;
-- 휴지통비우기
purge recyclebin;
-- 휴지통 거치지 않고 바로삭제
drop table emp2 purge;

--제약조건\
create table book(
    bookno number(5) primary key, --unique + not null , index 생성
    title varchar2(30) unique,  -- index 생성
    author varchar2(30),
    price number(7,2) check(price>0),
    pubdate date default sysdate
);

drop table book purge;
create table book(
    bookno number(5), --primary 설정 안한 버전
    title varchar2(30) unique,  -- index 생성 
    author varchar2(30), --NOT NULL속성도 있음
    price number(7,2) check(price>0),
    pubdate date default sysdate
);
--제약조건 만들기
ALTER TABLE book add constraint book_bookno_pk primary key(bookno); --book_bookno_pk는 제약조건 이름
--제약조건 제거
ALTER TABLE book DROP constraint book_bookno_pk; --book_bookno_pk는 제약조건 이름

desc book;
select * from book;
commit;
insert into book values(1,'html5','김길동',900,'17/1/5');
insert into book values(2,'jsp','김길동',900,'17/1/5');
insert into book values(3,'spring',null,null,'17/1/5');
insert into book values(4,'....',null,999,'17/1/5');
insert into book (bookno) values(5);
COMMIT;

SELECT* FROM DEPT;

DELETE DEPT WHERE DEPTNO = 50;

DROP TABLE DEPT2 PURGE;
CREATE TABLE DEPT2 AS SELECT *FROM DEPT;
DESC DEPT2;
-- 프라이머리 키 설정
ALTER TABLE DEPT2 ADD CONSTRAINT DEPT_DEPTNO_PK primary key(deptno);

DROP TABLE emp2 PURGE;
CREATE TABLE emp2 AS SELECT *FROM emp;
DESC emp2;
ALTER TABLE emp2 ADD CONSTRAINT emp_DEPTNO_PK primary key(empno);
- FK키
ALTER TABLE emp2 ADD CONSTRAINT emp_deptno_fK foreign key(deptno) references dept2;
ALTER TABLE emp2 ADD CONSTRAINT emp_mgr_fK foreign key(mgr)references emp2;
insert into emp2 (empno,ename,hiredate,sal,deptno,mgr) values(7777,'홍길동',sysdate,0,null,1111);
-- on delete set null 
ALTER TABLE emp2 ADD CONSTRAINT emp_deptno_fK foreign key(deptno) references dept2 on delete set null;
-- on delete cascade 자식도 같이 삭제
ALTER TABLE emp2 ADD CONSTRAINT emp_deptno_fK foreign key(deptno) references dept2 on delete cascade;

ALTER TABLE emp2 drop CONSTRAINT emp_deptno_fK;
ALTER TABLE emp2 drop CONSTRAINT emp_mgr_fK;

delete from dept2 where deptno = 20;
select * from emp2;

commit;

--트랜잭션 
--자식을 먼저 지우고 부모를 지운다.
drop table dept2 cascade constraint purge;

create table dept2 as select *from dept;
--창1
delete from dept2 where deptno=10;
--창2
update dept2 set loc= '...' where deptno=10;

-- 세이브 포인트를 활용한 롤백 
delete from dept2 where deptno = 10;
delete from dept2 where deptno = 20;
savepoint svp;
delete from dept2 where deptno = 30;

rollback to savepoint svp;
SELECT SYSDATE +1 FROM DUAL;