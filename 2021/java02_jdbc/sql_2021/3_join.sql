-- @ c:\lib\datatable.sql

select * from dept;
select * from emp;

-- ����Ŭ ����� �������� 
select ename,dname,dept.deptno
from emp,dept
where emp.deptno = dept.deptno;  -- �������� : fk = pk

select *
from emp,dept
where emp.deptno = dept.deptno;

select first_name,job_title,employees.job_id
from employees,jobs
where employees.job_id = jobs.job_id;


select ename,dname,d.deptno,sal
from emp e,dept d
where e.deptno = d.deptno;  -- �������� : fk = pk

-- sal�� 2500 �̻��� ����� �̸��� �μ��̸�,sal, �μ���ȣ  ����ϼ���. 
select ename,dname,d.deptno,sal
from emp e,dept d
where e.deptno = d.deptno and sal>=2500;  

select ename,dname,d.deptno,sal
from emp e,dept d
where e.deptno = d.deptno and  sal>=2500 ;  

-- �Ƚ����� (ǥ��) , from ���� , => join , where �������� on
-- inner join ��� ==> �������̺� ��� �ִ� ������ ���
select ename,dname,d.deptno,sal
from emp e join dept d
on e.deptno = d.deptno;

select ename,dname,d.deptno,sal
from emp e join dept d
on e.deptno = d.deptno 
where sal>=2500 ;  

select ename,dname,d.deptno,sal
from emp e join dept d
on e.deptno = d.deptno 
and  sal>=2500 ;  

-- outer join

select ename,dname,d.deptno,sal
from emp e,dept d
where e.deptno(+) = d.deptno; 


select ename,dname,d.deptno,sal
from emp e right outer join dept d
on e.deptno = d.deptno;

select ename,dname,d.deptno,sal
from emp e left outer join dept d
on e.deptno = d.deptno;

select ename,dname,d.deptno,sal
from emp e full outer join dept d
on e.deptno = d.deptno;

-- non-equi join  , equi join ==> fk=pk  , 

SELECT   grade, losal, hisal   FROM    salgrade;

SELECT ename,sal ,grade
from emp , salgrade
where sal BETWEEN  losal and hisal
order by ename;

SELECT ename,sal ,grade
from emp join salgrade
on sal BETWEEN  losal and hisal
order by ename;

SELECT *
from emp join salgrade
on sal BETWEEN  losal and hisal
order by ename;

SELECT emp.*,salgrade.grade
from emp join salgrade
on sal BETWEEN  losal and hisal
order by ename;

-- 3���� ���̺� ���� 
-- ename , dname,loc,sal , grade ���

SELECT ename , dname,loc,sal , grade
from emp,dept,salgrade
where emp.deptno=dept.deptno and sal BETWEEN losal and hisal;


SELECT ename , dname,loc,sal , grade
from emp,dept,salgrade
where emp.deptno=dept.deptno and sal BETWEEN losal and hisal;

SELECT ename , dname,loc,sal , grade
from emp
join dept
on emp.deptno=dept.deptno
join salgrade
on sal between losal and hisal
where sal >=2000
order by emp.deptno;


SELECT ename , dname,loc,sal , grade
from emp
join dept
on emp.deptno=dept.deptno
join salgrade
on sal between losal and hisal
and sal >=2000 --where and �Ѵ� �����ϴ�
order by emp.deptno;

select * from emp;

--SELF JOIN
--����� �̸��� ����� �̸� ���
select E.ename ���, M.ENAME ���
FROM EMP E, EMP M
WHERE E.MGR = M.EMPNO
ORDER BY 1;

-- ��簡 ���� KING������ �ƿ��� ������ ����ؼ� ��� ( �������� (+) ����. )
select E.ename ���, M.ENAME ���
FROM EMP E, EMP M
WHERE E.MGR = M.EMPNO(+)
ORDER BY 1;
-- ansi 
select E.ename ���, M.ENAME ���
FROM EMP E left join EMP M
on E.MGR = M.EMPNO
ORDER BY 1;

select * from emp;
-- ����� �޿��� ����� �޿����� ���� ��� ��� ����Ʈ
select E.ename||e.sal ���, M.ENAME||m.sal ��� ,e.ename better
FROM EMP E, EMP M
WHERE E.MGR = M.EMPNO(+) and e.sal > m.sal
ORDER BY 1;

select E.ename ��� ,nvl(to_char(m.ename),'CEO')���
FROM EMP E left join EMP M
on E.MGR = M.EMPNO;

--���� ���� where�� and ����
select empno,ename,dname,loc,sal
from emp e right join dept d
on e.deptno = d.deptno
where sal >2000; -- where ������ �ɰԵǸ� �ƿ������� �ΰ� �� ����� ���ܰ���

select empno,ename,dname,loc,sal
from emp e right join dept d
on e.deptno = d.deptno
AND sal >2000; -- AND�� �ְԵǸ� �ƿ������� ���� �� ���´�.

-- natural join
select empno,ename,dname,loc,sal
from emp natural join dept;

--using join
select empno,ename,dname,loc,sal
from emp join dept
using(deptno);


-- join: �÷����̱�
-- UNION: ROW���̱�