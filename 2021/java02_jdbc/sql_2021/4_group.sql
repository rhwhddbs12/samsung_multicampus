4일차

select E.ename||to_date(to_char(e.hiredate,'yy-mm-dd')) 사원, M.ENAME||to_date(to_char(m.hiredate,'yy-mm-dd')) 상사, e.ename 먼저
FROM EMP E, EMP M
WHERE E.MGR = M.EMPNO and e.hiredate<m.hiredate;

select * from emp;


-- 6장 집계함수 그룹함수
--lower 소문자 변환 함수 
select ename, sal,lower(ename), hiredate, to_char(hiredate,'mm')  
from emp;

--소수점 이하 컨트롤
select round(avg(sal),2)
from emp;

select lower(ename), avg(sal)
from emp;

select sum(sal),count(sal), avg(sal),max(sal),min(sal)
from emp;

select count(*) from emp;
--중복행 제거
select count(distinct job) from emp;

select round(avg(sal)) from emp where deptno = 10;

-- 부서별, 그룹바이 함수 쓸땐 셀렉트에 그룹바이에 참여한 컬럼만 쓸 수 있음. +집계함수
select emp.deptno 부서,dname, ROUND(avg(sal)) 평균급여, MAX(SAL) 최대급여,MIN(SAL) 최소급여
from EMP join dept
on emp.deptno = dept.deptno
group by emp.deptno, dname  --여기에 컬럼을 써야지만 셀렉트로 조회할수있다
order by emp.deptno;

--LOC 도시이름별 평균 급여를 구하세요.

select loc 지역 ,round(avg(sal)) 평균
from emp join dept
on emp.deptno = dept.deptno
group by loc;

--EMP테이블에서 월별 입사자 수
SELECT to_char(hiredate,'mm') 월별 ,count(*) 입사자수
from emp
group by to_char(hiredate,'mm')
order by 1;

--부서이름별 평균 급여가 2000 이상인 리스트.
select loc 지역 ,round(avg(sal)) 평균
from emp join dept
on emp.deptno = dept.deptno
group by loc
having round(avg(sal)) > 2000  --그룹바이 조건은 WHERE가 아닌 HAVING절을 사용해야한다.
order by 2;

select dname 지역 ,round(avg(sal)) 평균
from emp join dept
on emp.deptno = dept.deptno
where sal >2000 -- 2000이상인것만 집계가 들어간다.
group by dname
having round(avg(sal)) > 2000  --그룹바이 조건은 WHERE가 아닌 HAVING절을 사용해야한다.
order by 2;

--rollup and cube

select deptno, job, sum(sal)
from emp 
group by rollup(deptno,job)
order by 1,2;

--8 장 subquery

--ford 사원보다 급여를 많이 받는 사원 리스트

SELECT * FROM EMP WHERE ENAME = 'FORD';

SELECT *
FROM EMP
WHERE SAL > (SELECT SAL FROM EMP WHERE ENAME = 'FORD');

-- 평균 급여보다 적게 받는 사원 리스트
SELECT ename||sal
FROM EMP
WHERE SAL < (SELECT avg(SAL) FROM EMP);

-- 급여가 제일 적은 사원
select *
from emp
where sal =  (SELECT min(SAL) FROM EMP);

--스칼라 =단일값 , 벡터= 다중값
SELECT min(sal) FROM emp;
SELECT min(sal) FROM emp group by deptno;

-- 서브쿼리 where = 에는 단일 값이 와야한다. where in 뒤에는 여러 값이 와도 된다.
--부서별 처저 급여 리스트
select deptno,ename,sal
from emp
where sal in  (SELECT min(SAL) FROM EMP group by deptno);
--ex 위
select deptno,ename,sal
from emp
where sal in  (800,950,1300);

--부서별 최고 급여를 받는 사람 리스트
select deptno,ename,sal
from emp
where (deptno,sal) in (SELECT deptno, max(SAL) FROM EMP group by deptno);
--ex 위
select deptno,ename,sal
from emp
where (deptno,sal) in  ((30,2850),(20,3000),(10,5000));

SELECT DEPTNO,ENAME,SAL
FROM EMP
WHERE DEPTNO < ANY(10,30);

SELECT DEPTNO,ENAME,SAL
FROM EMP
WHERE DEPTNO < ALL(20,30);

--상관관계 쿼리문
--자신이 속한 부서의 편균 급여보다 적은 사원 리스트
SELECT*
FROM EMP E
WHERE SAL <(SELECT AVG(SAL) FROM EMP WHERE DEPTNO=E.DEPTNO);

--페이징 처리
select * from 
( SELECT rownum row#,deptno , job,  ename,     hiredate 입사일 ,    sal as 급여
  from (SELECT * from emp order by sal desc))
where row# between 11 and 14;

select * from 
( SELECT rownum row#,deptno , job,  ename,     hiredate 입사일 ,    sal as 급여
  from (SELECT * from emp order by sal desc))
where row# between &start and &and;

-- 9장 집합연산자
--union은 컬럼의 갯수가 일치해야 함!

-- UNION: 합하고 중복은 하나만 
select job from emp where deptno = 10
union
select job from emp where deptno = 20;
-- UNIONALL: 중복까지 합침
select job from emp where deptno = 10
union ALL
select job from emp where deptno = 20;

--INTERSECT: 교집합만 뽑아냄
select job from emp where deptno = 10
intersect
select job from emp where deptno = 20;

--MINUS: 차집합
select job from emp where deptno = 10
minus
select job from emp where deptno = 20;

