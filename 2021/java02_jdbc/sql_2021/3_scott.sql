SELECT rownum row#, empno, ename, 
FROM (SELECT * FROM EMPLOYEES ORDER BY SALARY DESC)
WHERE SALARY BETWEEN 6 AND 10;

insert into emp
values(1111,'고','ANALYST', 7566, TO_DATE('1987/04/19','YYYY/MM/DD'),3000,NULL,20);

COMMIT; -- COMMIT 를해줘야만 다른 클라이언트에서도 해당 데이터를 볼 수 있음. 커밋안하면 커멘트창에서는안보임.
rollback; -- COMMIT 안했으면 변경사항 롤백  단, 커밋했으면 롤백 안됨.

insert into emp
values(2222,'김','ANALYST', 7566, TO_DATE('1987/04/19','YYYY/MM/DD'),NULL,NULL,NULL);

SELECT *
FROM EMP;

insert into emp
values(2222,'김','ANALYST', 7566, TO_DATE('12/16/20','MM/DD/YY'),NULL,NULL,NULL);

SELECT ENAME, NVL(SAL+COMM,0)급여
FROM EMP;

--사용자 함수 생성
create or replace function tax(a number) return number
is
 v_tax number;
begin 
 v_tax := a * 0.01;
 return v_tax;
END;
/

--DDL AUTO COMMIT
SELECT ENAME, SAL,TAX(SAL) 세금
FROM EMP;
-- 영문 1바이트, 한글 3바이트(보통 언어에선 2바이트인데 오라클에선 3바이트), 특수문자 1바이트
select length('hello'), length('안녕~') from dual;
select lengthb('hello#'), lengthb('안녕~') from dual;

--NULLIF 
SELECT NULLIF('11','11') FROM DUAL; --값이 같으면 널
SELECT NULLIF('11','111') FROM DUAL; --값이 다르면 앞에것 출력

-----------------------------------------------4단원 연습문제
--1.
select *from emp;
--2
select hiredate 
from emp;
--3
SELECT EMPNO , ENAME, SAL, SAL*1.15 NewSalary
from emp;

--4
SELECT EMPNO , ENAME, SAL, SAL*1.15 NewSalary, sal*1.15-sal Increase
from emp;

--5

--@ C:\LIB\DATATABLE.SQL

SELECT * FROM EMP;


--------------------------------------------------JOIN--------------------------------

SELECT ENAME , DNAME,D.DEPTNO --그냥 DEPTNO만쓰면안됨 D,E양쪽 둘 다 있기때문.
FROM EMP E, DEPT D
WHERE E.DEPTNO = D.DEPTNO;
-- 오라클 조인 방식
select employee_id,E.job_id,job_title
from employees E JOIN jobs J
ON E.job_id = J.job_id;


--ANSIJOIN(표준) , FROM => JOIN, WHERE 조인조건 ON
--INNER JOIN 방식 --> 양쪽 테이블에 모두 있는 내용
select employee_id,E.job_id,job_title
from employees E JOIN jobs J
ON E.job_id = J.job_id;

-- sal이 2500 이상인 사원 출력

select employee_id,E.job_id,job_title,salary
from employees E, jobs J
where E.job_id = J.job_id and e.salary>=2500
order by salary;

-- ANSI 표준
select employee_id,E.job_id,job_title,salary
from employees E JOIN jobs J
ON E.job_id = J.job_id -- 똑같이 and로 해도됨.
WHERE e.salary>=2500;

--outer join
select ename,dname,d.deptno,sal
from emp E, dept D
where E.deptno = D.deptno(+);


