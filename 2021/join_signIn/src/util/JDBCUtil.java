package util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class JDBCUtil {

	public static Connection getConnection() {
		String driver = "oracle.jdbc.driver.OracleDriver";
		String url ="jdbc:oracle:thin:@127.0.0.1:55003:xe";
		String user = "system";
		String pw = "1234";
		
		Connection con = null;
		
		try {
			//1. Driver class로딩
			Class.forName(driver);
			//2. 로딩된 Driver클래스를 이용해 connection요청(url,user,pwd)
			con = DriverManager.getConnection(url,user,pw);
			
		}catch (SQLException e) {
			e.printStackTrace();
		}catch(ClassNotFoundException e) {
			System.out.println("JDBC driver 확인");
		}catch(Exception e) {
			System.out.println(e);
		} 
		return con;
}
	
	public static void close(Connection con, Statement st, ResultSet rs){
		//7. 자원정리(connection, statement, resultset)
		try {
			if(rs != null) rs.close();
			if(st != null) st.close();
			if(con != null) con.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}
	
}
