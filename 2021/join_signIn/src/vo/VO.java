package vo;  //데이터 운반객체 / 밸류 오브젝

import java.io.Serializable;  // 데이터를 넘겨줄 수 있게 해주는거같음

public class VO implements Serializable{
	private String U_ID; 	//varchar2(14)
	private String U_PW;		//varchar2(13)
	public VO() {
		super();
	}
	public VO(String u_ID, String u_PW) {
		super();
		U_ID = u_ID;
		U_PW = u_PW;
	}
	public String getU_ID() {
		return U_ID;
	}
	public void setU_ID(String u_ID) {
		U_ID = u_ID;
	}
	public String getU_PW() {
		return U_PW;
	}
	public void setU_PW(String u_PW) {
		U_PW = u_PW;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((U_ID == null) ? 0 : U_ID.hashCode());
		result = prime * result + ((U_PW == null) ? 0 : U_PW.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		VO other = (VO) obj;
		if (U_ID == null) {
			if (other.U_ID != null)
				return false;
		} else if (!U_ID.equals(other.U_ID))
			return false;
		if (U_PW == null) {
			if (other.U_PW != null)
				return false;
		} else if (!U_PW.equals(other.U_PW))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "VO [U_ID=" + U_ID + ", U_PW=" + U_PW + "]";
	}
	
}
