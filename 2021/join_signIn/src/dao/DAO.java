package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;


import util.JDBCUtil;
import vo.VO;


public class DAO {
	public int join(VO vo) {  //파라미터 세개 값이 들어가는것보다 이렇게 객체하나로 많이씀
		Connection con = null;
		Statement st = null; // SQL구문을 처리
		PreparedStatement ps = null;
		ResultSet rs = null; // Select타입의 결과 
		int row = 0; 
		
		String sql = "insert into dept(deptno,dname,loc) values(?,?)";

		try {
			con = JDBCUtil.getConnection();
			ps = con.prepareStatement(sql);
			//? 세팅작업
			ps.setString(1,vo.getU_ID());
			ps.setString(2,vo.getU_PW());
			
			row = ps.executeUpdate(); //insert, delete, update
			//결과 값 핸들
			
		}catch(Exception e){
			e.printStackTrace();
		}finally {
			JDBCUtil.close(con, ps, rs);
		}
		return row;
	}
	
	
	public void login(VO vo) {
		
		Connection con = null;
		PreparedStatement ps = null;
		
		ResultSet rs = null; // Select타입의 결과 
		int row = 0; 
		
		String sql = "select * from users  ";

		try {
			
			con = JDBCUtil.getConnection();
			ps = con.prepareStatement(sql);
			//? 세팅작업
			
			ps.setString(1, vo.getU_ID());
			ps.setString(2, vo.getU_PW());
			
			rs = ps.executeQuery();
			//결과 값 핸들
			
		}catch(Exception e){
			e.printStackTrace();
		}finally {
			JDBCUtil.close(con, ps, rs);
		}
	}
	public VO getUser(String U_ID) {
		Connection con = null;
		PreparedStatement ps = null;

		ResultSet rs = null;
		int row = 0;

		String sql = "select * from users where U_id = ?";
		VO vo = null;

		try {
			con = JDBCUtil.getConnection();
			ps = con.prepareStatement(sql);
			// ? 세팅작업
			ps.setString(1, U_ID);
			rs = ps.executeQuery(); // select
			// 결과값 핸들링
			while (rs.next()) {
				vo = new VO();
				vo.setU_ID(rs.getString("U_id"));
				vo.setU_PW(rs.getString("U_PW"));
			
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			JDBCUtil.close(con, ps, rs);
		}

		return vo;
	}
	
}

