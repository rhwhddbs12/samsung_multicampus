package service;

import java.util.List;

import dao.DAO;
import vo.VO;

public class ServiceImpl implements Service {

	DAO dao = new DAO();
	@Override
	public int join(VO vo) {
		return dao.join(vo);
	}
	@Override
	public void login(VO vo) {
		// TODO Auto-generated method stub
		dao.login(vo);
	}
	@Override
	public VO getUser(String U_ID) {
		// TODO Auto-generated method stub
		return dao.getUser(U_ID);
	}
	
}
