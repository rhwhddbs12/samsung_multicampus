package service;

import java.util.List;

import vo.VO;

public interface Service {
	
	public int join(VO vo);
	public void login(VO vo);
	public VO getUser(String U_ID);
	
}
