package userExercise;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import util.JDBCUtil;
import vo.DeptVO;

public class UserDAO {
	public int insertUser(String id, String password, String roll, String name) {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		int row = 0;
		String sql = "insert into myuser(id, password, roll, name) values(?, ?, ?, ?)";

		try {
			con = JDBCUtil.getConnection();
			ps = con.prepareStatement(sql);
			// ? 세팅작업
			ps.setString(1, id);
			ps.setString(2, password);
			ps.setString(3, roll);
			ps.setString(4, name);

			row = ps.executeUpdate(); // insert, delete, update
			// 결과값 핸들링

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			JDBCUtil.close(con, ps, rs);
		}
		return row;
	}
	

	public int insertUser(UserVO uvo) {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		int row = 0;
		String sql = "insert into myuser(id, password, roll, name) values(?, ?, ?, ?)";

		try {
			con = JDBCUtil.getConnection();
			ps = con.prepareStatement(sql);
			// ? 세팅작업
			ps.setString(1, uvo.getId());
			ps.setString(2, uvo.getPassword());
			ps.setString(3, uvo.getRoll());
			ps.setString(4, uvo.getName());

			row = ps.executeUpdate(); // insert, delete, update
			// 결과값 핸들링

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			JDBCUtil.close(con, ps, rs);
		}
		return row;
	}
	
	public void updateUser(UserVO uvo) {
		Connection con = null;
		PreparedStatement ps = null;

		ResultSet rs = null;
		int row = 0;

		String sql = "update myuser set id=?, password=?, roll=? where name=?";

		try {
			con = JDBCUtil.getConnection();
			ps = con.prepareStatement(sql);
			// ? 세팅작업
			ps.setString(1, uvo.getId());
			ps.setString(2, uvo.getPassword());
			ps.setString(3, uvo.getRoll());
			ps.setString(4, uvo.getName());

			row = ps.executeUpdate(); // insert, delete, update
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			JDBCUtil.close(con, ps, rs);
		}
	}
	
	public void deleteUser(String id) {
		Connection con = null;
		PreparedStatement ps = null;

		ResultSet rs = null;
		int row = 0;

		String sql = "delete from myuser where id=?";

		try {
			con = JDBCUtil.getConnection();
			ps = con.prepareStatement(sql);
			// ? 세팅작업
			ps.setString(1, id);

			row = ps.executeUpdate(); // insert, delete, update
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			JDBCUtil.close(con, ps, rs);
		}
	}
	
	public UserVO getUser(String id) {
		Connection con = null;
		PreparedStatement ps = null;

		ResultSet rs = null;
		int row = 0;

		String sql = "select * from myUser where id = ?";
		UserVO uvo = null;

		try {
			con = JDBCUtil.getConnection();
			ps = con.prepareStatement(sql);
			// ? 세팅작업
			ps.setString(1, id);
			rs = ps.executeQuery(); // select
			// 결과값 핸들링
			while (rs.next()) {
				uvo = new UserVO();
				uvo.setId(rs.getString("id"));
				uvo.setPassword(rs.getString("password"));
				uvo.setRoll(rs.getString("roll"));
				uvo.setName(rs.getString("name"));
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			JDBCUtil.close(con, ps, rs);
		}

		return uvo;
	}
	
	public List<UserVO> getUserAll() {
		Connection con = null;
		PreparedStatement ps = null;

		ResultSet rs = null;
		int row = 0;

		String sql = "select * from myuser order by id";
		List<UserVO> list = new ArrayList<UserVO>();

		try {
			con = JDBCUtil.getConnection();
			ps = con.prepareStatement(sql);
			// ? 세팅작업
			rs = ps.executeQuery(); // select
			// 결과값 핸들링
			while (rs.next()) {
				UserVO uvo = new UserVO();
				uvo.setId(rs.getString("id"));
				uvo.setPassword(rs.getString("password"));
				uvo.setRoll(rs.getString("roll"));
				uvo.setName(rs.getString("name"));
				list.add(uvo);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			JDBCUtil.close(con, ps, rs);
		}

		return list;
	}
	
}
