package userExercise;

import java.util.List;

import vo.DeptVO;

public interface UserService {

	public int insertUser(String id, String password, String roll, String name);
	public int insertUser(UserVO uvo);
	public void updateUser(UserVO uvo);
	public void deleteUser(String id);
	public UserVO getUser(String id);
	public List<UserVO> getUserAll();
}
