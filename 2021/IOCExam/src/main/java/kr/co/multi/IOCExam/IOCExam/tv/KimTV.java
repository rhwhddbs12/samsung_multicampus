package kr.co.multi.IOCExam.IOCExam.tv;

public class KimTV implements TV {
	public KimTV() {
		System.out.println("KimTV 생성! ");
	}
	@Override
	public void turnOn() {
		System.out.println("전원을 켜다.");
		
	}

	@Override
	public void turnOff() {
		System.out.println("전원을 끄다.");
	}

	@Override
	public void volumeUp() {
		System.out.println("소리를 높이다.");
		
	}

	@Override
	public void volumeDown() {
		System.out.println("소리를 낮추다.");
		
	}
	
}
