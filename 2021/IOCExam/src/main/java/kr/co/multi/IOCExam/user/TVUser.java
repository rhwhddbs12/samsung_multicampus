package kr.co.multi.IOCExam.user;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import kr.co.multi.IOCExam.IOCExam.tv.TV;
public class TVUser {
	public static void main(String[] args) {
		ApplicationContext factory = 
				new ClassPathXmlApplicationContext("applicationContext.xml");
	
		TV tv = factory.getBean("j",TV.class);
	
		tv.turnOff();
		tv.turnOn();
		tv.volumeDown();
		tv.volumeUp();
	}
}
