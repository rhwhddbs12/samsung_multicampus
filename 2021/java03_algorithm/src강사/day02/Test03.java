package day02;

public class Test03 {

	public static void main(String[] args) {
       System.out.println(sum1(5));
       System.out.println(f(5));
       System.out.println(factorial(10));
       System.out.println(gcd(9,24));
       System.out.println(gcd(24,9));
	}

	public static int sum1(int n) {
		int sum = 0;
		for(int i=1;i<=n;i++) {
			sum += i;
		}
		return sum;
	}
	public static int f(int n) { // n n-1 n-2 .. 1
          if(n==1)  return 1;
          return n + f(n-1);
              // 5 + f(4) 
                //   4+f(3)
                 //   3+f(2)
                  //   2+f(1)
                     //    1
	}
	public static int factorial(int n) { // n n-1 n-2 .. 1   ==>  1*2*3*4*
        if(n==1)  return 1;
        return n * factorial(n-1);
	}
	// 최대공약수 재귀 함수
	public static int gcd(int x,int y) {
		if(y == 0) return x;
		return gcd(y, x % y);
	}
	
}


