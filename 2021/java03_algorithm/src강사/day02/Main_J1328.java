package day02;
import java.io.BufferedReader;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

//이중 for문으로 통과 못함
public class Main_J1328 {

	public static void main(String[] args) throws IOException{

		System.setIn(new FileInputStream("input/1328.txt"));
//		Scanner sc = new Scanner(System.in);
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		StringBuilder sb = new StringBuilder();
		int N = Integer.parseInt(br.readLine().trim());
		
		int[] b = new int[N];
		//int[] r = new int[N];
		
		for(int i=0;i<N;i++) {
			b[i] = Integer.parseInt(br.readLine().trim());
		}
		
		A:for(int i=0 ;i<N;i++) {
			for(int j=i+1;j<N;j++) {
				if(i<j && b[i] < b[j]) {
					//System.out.println(j+1);
					sb.append(j+1).append("\n");
					continue A;
				}
			}
//			System.out.println(0);
			sb.append(0).append("\n");
		}
		
		System.out.println(sb.toString());
		br.close();
		br = null;
	}

}

/*
 * 입력값 6 3 2 6 1 1 2
 * 
 * 
 * 출력예 3 3 0 6 6 0
 */