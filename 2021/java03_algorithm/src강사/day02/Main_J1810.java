package day02;

import java.io.FileInputStream;
import java.util.Scanner;

//1810 : 백설공주(Snow White) 재귀함수
public class Main_J1810 {
	static int[] c = new int[9];
	static boolean[] V = new boolean[9];
	static int N = c.length;
	
	public static void main(String[] args) throws Exception {
		System.setIn(new FileInputStream("input/1810.txt"));
		Scanner sc = new Scanner(System.in);

		
		for (int i = 0; i < N; i++) {
			c[i] = sc.nextInt();
		}
		
        f(0,0,0);
        
		sc.close();
		sc = null;
	}
	
	
	public static void f(int idx,int cnt,int sum) {
		if(sum > 100) return;
		if(cnt==7 && sum ==100) {
			for(int i=0;i<N;i++) {
				if(V[i]) System.out.println(c[i]);
			}
			return;
			//System.exit(0); //App 종료 
		}
		if(cnt==7 || idx == 9) return;
		
		V[idx] = true;
		f(idx+1,cnt+1,sum+c[idx]);
		
		V[idx] = false;
		f(idx+1,cnt,sum);
		
	}
	
	


}
