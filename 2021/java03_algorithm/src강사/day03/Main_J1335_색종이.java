package day03;

import java.io.FileInputStream;
import java.util.Scanner;

public class Main_J1335_색종이 {  //B2630

	static int white = 0;
	static int blue = 0;
	
	public static void main(String[] args) throws Exception {
		System.setIn(new FileInputStream("input/input.txt"));
		Scanner sc = new Scanner(System.in);
		
		int N = sc.nextInt();
		int[][] arr = new int[N][N]; 
		
		for(int i=0;i<N;i++) {
			for(int j=0;j < N;j++) {
				arr[i][j] = sc.nextInt();
			}
		}
		
		f(arr,0,0,N);
		
		System.out.println(white);
		System.out.println(blue);
		
	}

	static void f(int[][] arr, int x, int y, int size) {
		int sum = 0;
		
		for(int i=x;i<x+size;i++) {
			for(int j=y;j<y+size;j++) {
				sum += arr[i][j];
			}
		}
		if(sum == size*size) blue++;
		else if(sum ==0) white++;
		else {
			int newsize = size/2;
			
			f(arr,x,y,newsize);
			f(arr,x,y+newsize,newsize);
			f(arr,x+newsize,y,newsize);
			f(arr,x+newsize,y+newsize,newsize);
			
		}
	}
}
