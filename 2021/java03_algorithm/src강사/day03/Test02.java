package day03;

import java.util.Arrays;

// 제출 
public class Test02 {

	static String[] arr = {
			"static","class","package","if","void",
			"int","byte","long","new","final",
			"public","reurn","break","continue","else",
			"switch","do","while","for","booleane"
	};
	static int N = arr.length;
	
	public static void main(String[] args) {
		String key = "new";
		Arrays.sort(arr);
		int idx =  f(arr,0,N-1,key);
		System.out.printf("%s ==> %d %n",key,idx);
		
//		System.out.println("C".compareTo("A"));  
	}
	
	public static int f(String[] arr,int start ,int end,String key) {
		if(start > end) return -1;
		
		int center = (start + end) / 2;
		if(arr[center].compareTo(key) == 0) return center;
		if(arr[center].compareTo(key) < 0)   return f(arr,center+1,end,key);
		else return f(arr,start,center -1,key);   
	}
}
