package day01;

import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

//1810 : 백설공주(Snow White)
public class Main_J1810 {
	public static void main(String[] args) throws Exception {
		//System.setIn(new FileInputStream("input/1810.txt"));
		Scanner sc = new Scanner(System.in);

		int[] c = new int[9];
		//System.out.println("총부분집합 수  " + (1 << c.length));
		//System.out.println("7난장이 경우의수 " + (1 << 7));
		for (int i = 0; i < c.length; i++) {
			c[i] = sc.nextInt();
		}
		
		print(c, c.length).forEach(i->System.out.println(i));

		sc.close();
		sc = null;
	}
	
	public static List<Integer> print(int[] c, int n) {
		List<Integer> list = null;
		for (int i = 0; i < 1<<n ; ++i) {
			list = new ArrayList<Integer>();
			int sum = 0;
			if(count(i) == 7) {
				for(int j=0; j<n;j++) {
					if ((i & (1 << j)) != 0) {
						list.add(c[j]);
						sum += c[j];
					}
				}
				if(sum == 100) return list;
			}
		}
		return list;
	}
	
	

	// 부분집합 원소 갯수 카운팅
	public static int count(int v) {
		int count = 0;
		while (v > 0) {
			if ((v & 1) == 1)
				count++;
			v = v >> 1;
		}
		return count;
	}

}
