package day01;

import java.util.Arrays;

import util.Sort;

public class Test01_sort {
	public static void main(String[] args) {
		int[] arr = new int[1000000];
		long start =0;
		long end = 0;
		
		for(int i=0;i<arr.length;i++) {
			arr[i] = (int) (Math.random()*10000+1);
		}

//		System.out.println(Arrays.toString(arr));
		start = System.currentTimeMillis();
		Sort.selectionSort(arr);
//		Arrays.sort(arr);
//		Sort.bubbleSort(arr);
//		Sort.quickSort(arr, 0, arr.length-1);
		end = System.currentTimeMillis();
//		System.out.println(Arrays.toString(arr));

		System.out.printf("selectionSort :  %d ms %n",end-start);
//		System.out.printf("Quick sort :  %d ms %n",end-start);
//		System.out.printf("bubble Sort :  %d ms %n",end-start);
//		System.out.printf("quickSort Sort :  %d ms %n",end-start);
		

	}
}
