package day02;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {
	static int sum;
	static int count;
	static int [] a = new int[9];
	static int N = a.length;
	public static void main(String[] args) throws Exception {
		
		System.setIn(new FileInputStream("input/1810.txt"));
		Scanner sc = new Scanner(System.in);
	
		
		for(int i =0; i< a.length; i++) {
			a[i] = sc.nextInt();
		}
		f((1<<N));
		
		sc.close();
		sc=null;
	}
	public static void f(int all) {
		int count=0;
		sum = 0;
		List<Integer> r = new ArrayList<Integer>();
		for(int j =0;j<a.length;j++) {
			if(( all & (1<<j) ) != 0 ) { 
				count++;
				sum+=a[j];
				r.add(a[j]);
			}	
		}
		System.out.println(r.toString());
		if(count==7 && sum==100) {
			for(int x =0; x<r.size();x++) {
				System.out.println(r.get(x));
			}
		}else {
			f(all-1);
		}
		
	}
	
}
