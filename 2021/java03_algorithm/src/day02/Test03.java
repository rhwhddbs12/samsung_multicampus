package day02;

public class Test03 {

	public static void main(String[] args) {
		System.out.println(sum1(10));
		System.out.println(f(10));
		System.out.println(factorial(10));
		System.out.println(gcd(24,3));
		System.out.println(gcd(3,24));
	}
	
	public static int sum1(int n) {
		
		int sum = 0;
		for(int i = 1 ;i<=n ;i ++) {
			sum+=i;
		}
		return sum;
	}
	//n=1이 될때까지 계속 더함
	public static int f(int n) {
		if(n==1) return 1;
		return n + f(n-1);
		
	}
	//n=1이 될때까지 계속 곱함
	public static int factorial(int n) {
		if(n==1) return 1;
		return n * f(n-1);
		
	}
	//최대공약수를 구하는 재기함수
	public static int gcd(int x,int y) {
		if(y==0)return x;
		
		return gcd(y,x%y);				
	}
	
	
}

