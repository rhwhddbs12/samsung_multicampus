package day02;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStreamReader;
import java.util.Scanner;

public class Main_1318 {

	public static void main(String[] args) throws Exception {
		
		System.setIn(new FileInputStream("input/1328.txt"));
//		Scanner sc = new Scanner(System.in);
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		StringBuilder sb = new StringBuilder();
//		int N = sc.nextInt();
		int N = Integer.parseInt(br.readLine().trim());
		
		int[] b = new int[N];
		int r[] = new int[N];
		
		for(int i =0;i<N; i++) {
//			b[i] = sc.nextInt();
			b[i] = Integer.parseInt(br.readLine().trim());
		}

		A:for(int i = 0; i < N ; i++) {
			for(int j = i+1;j<N;j++) {
				if(i<j && b[i] <b[j]) {
//					System.out.println(j+1);
					sb.append(j+1).append("\n"); //출력문들을 sb에 몰아넣음으로서 성능향상
					continue A;
				}
			}
//			System.out.println("0");
			sb.append(0).append("\n");
		}
		System.out.println(sb.toString());
//		sc.close();
//		sc = null;
		br.close();
		br = null;
	}

}


