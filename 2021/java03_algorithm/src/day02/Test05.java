package day02;

public class Test05 {
	static int[] num = {3,5,7};
	static int N = num.length;
    static boolean[] V = new boolean[N];
    
	public static void main(String[] args) {
		System.out.println("부분집합 갯수 "+(1<<num.length));
		f(0,0);
	}
	
	public static void f(int idx,int cnt) {
		if(idx == N) {
			for(int i=0;i<N;i++) {
				if(V[i] ) System.out.print(num[i]+" ");
			}
			System.out.println("  원소의 개수  "+cnt );
			return;
		}
		V[idx] = true;
        f(idx+1,cnt+1); 
		V[idx] = false;
		f(idx+1,cnt);
		
	}
}



