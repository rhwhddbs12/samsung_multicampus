/**
  스와핑 횟수를 줄여서 효율성있게 정렬하여 작성한 코드
  셀렉션 소트는시간이 한참걸려서 사실상 안씀
  머지소트와 퀵소트를 쓴다 ! 
 */
package util;

import java.util.Arrays;

public class Sort {
	
	//Sort클래스 내부에서만 쓰기위해 프라이빗 처리
	public static void swap(int[] arr,int i,int j) {
		int temp = arr[j];
		arr[j] = arr[i];
		arr[i]=temp;
		
	}
	
	//O(n2)
	public static void selectionSort(int[] arr) {
		
		int count = 0;
		for(int i=0; i<arr.length-1;i++) {
			int minidx = i;
			for(int j=i+1; j<arr.length;j++) {
				if(arr[minidx] > arr[j]) {
					minidx = j;
				}
			}
			if(minidx!=i) {
				if(arr[i]>arr[minidx]) {
					swap(arr,i,minidx);
					count++;
				}
			}
		}
		
		System.out.printf("count:%d \n",count);
		System.out.println(Arrays.toString(arr));
	}

	//O(n2) 
	public static void bubbleSort(int[] arr) {
		final int size = arr.length;
		
		int temp = 0;
		for(int i =0 ; i< arr.length-1 ;i ++) {
			for(int j =0 ; j< arr.length-1-i ;j ++) {
				if(arr[j]>arr[j+1]) {
					swap(arr,j,j+1);
				}
			}
		}
	}
	
	
}




//if(arr[i]>arr[j]) {
//	temp = arr[j];
//	arr[j] = arr[i];
//	arr[i]=temp;
//	count++;
//}