package day01;

public class Test03 {

	public static void main(String[] args) {
		char[] c = {'A','B','C','D'}; //1<<4 부분집합의 개수 ->16 -> 2의 4승
		
		print(c,c.length,2);

	}
	
	public static void printAll(char[] c, int n) {
		for(int i=0; i<(1<<n); i++) {
			System.out.print("{");
			
			for(int j =0;j<n;j++) {
				if(( i & (1<<j) ) != 0 ) System.out.printf("%c", c[j]);
			}
			
			System.out.println("}");
		}
	}
	
	public static void print(char[] c, int n, int count) {
		for(int i=0; i<(1<<n); i++) {
			if(count(i) == count) {
			System.out.print("{");
			
			for(int j =0;j<n;j++) {
				if(( i & (1<<j) ) != 0 )
					System.out.printf("%c", c[j]);
			}
			
			System.out.println("}");
			}
		}
	}
	
	
	public static int count(int v) {
		int count=0; 
		
		while(v >0) {
			if((v&1) ==1) count++;
			v = v >> 1;
		}
		
		return count;
	}
	
	
}
