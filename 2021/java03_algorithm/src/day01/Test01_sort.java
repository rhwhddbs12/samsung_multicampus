package day01;

import java.util.Arrays;

import util.Sort;

public class Test01_sort {

	public static void main(String[] args) {
	
		long start = 0;
		long end =0;
		
		int[] arr2 = new int[1000];
		for(int i = 0; i<arr2.length;i++){
			arr2[i] = (int)(Math.random()*10000+1);
		}
		System.out.println(Arrays.toString(arr2));
		start = System.currentTimeMillis();
//		Arrays.sort(arr2);
//		Sort.selectionSort(arr2);
//		Sort.bubbleSort(arr2);
		Sort.quick(arr2,0,arr2.length-1);
		
		end = System.currentTimeMillis();
		
//		System.out.printf("selectionSort: %d ms %n",end-start); //데이터가 커지면시간이 한참걸림
		System.out.printf("ArraysSort: %d ms %n",end-start);// 퀵소트로 하면 시간이 훨씬 빠르다.
		System.out.printf("quickSort: %d ms %n",end-start);// 퀵소트로 하면 시간이 훨씬 빠르다.
	}
	

}
