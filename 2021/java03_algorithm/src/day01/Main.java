/**
 * 경우의 수 구하기 = 1<<n  n으로 만들 수 있는 경우의 수
 */

package day01;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) throws Exception {
		
		int [] a = new int[9];
		int sum;
		System.setIn(new FileInputStream("input/input.txt"));
		Scanner sc = new Scanner(System.in);
	
		
		for(int i =0; i< a.length; i++) {
			a[i] = sc.nextInt();
		}
		for(int i=0; i<(1 <<a.length); i++) {		
			int count=0;
			sum = 0;
			List<Integer> r = new ArrayList<Integer>();
			for(int j =0;j<a.length;j++) {
				if(( i & (1<<j) ) != 0 ) { 
					count++;
					sum+=a[j];
					r.add(a[j]);
				}	
			}
			if(count==7 && sum==100) {
				for(int x =0; x<r.size();x++) {
					System.out.println(r.get(x) );
				}
			}
			
		}
		sc.close();
		sc=null;
	}
}
