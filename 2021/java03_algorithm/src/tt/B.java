package tt;

public class B {
	private String name;
	private String id;
	private int age;
	
	public B() {
		super();
	}

	public B(String name, String id, int age) {
		super();
		this.name = name;
		this.id = id;
		this.age = age;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	@Override
	public String toString() {
		return "B [name=" + name + ", id=" + id + ", age=" + age + "]";
	}
	
	
	
}
