/////compareTo()


package day03;

import java.util.Arrays;

public class Test02 {
	static String[] arr = {
			"static","class","package","if","void",
			"int","byte","long","new","final",
			"public","return","break","continue","else",
			"switch","do","while","for","boolean"
	};
	static int N= arr.length;
	
	public static void main(String[] args) {
		String key = "new";
		Arrays.sort(arr); //배열 순서대로 정
		//System.out.print(Arrays.toString(arr));
		int idx = binSearch(arr,0,N-1,key);
		System.out.printf("%s==> %d %n",key,idx);
		

	}
	public static int binSearch(String[] arr,int start,int end,String key) {
		if(start > end) return -1; // 함수 종료 시점(무한루프 방지) 이부분 중요! 
		
			int center = (start + end) / 2;
			// 정답을 찾은경우
			if( arr[center].compareTo(key) ==0 ) return center;
			// 센터 오른쪽으로 쭉 값을 찾는다.
			if(arr[center].compareTo(key) < 0 ) return binSearch(arr,center+1,end,key);
			// 센터 왼쪽으로 쭉 값을 찾는다.
			else return binSearch(arr,start,center -1,key);
			
	
	}
}
