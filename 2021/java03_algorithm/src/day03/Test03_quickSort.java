package day03;

import java.util.Arrays;

import util.Sort;

public class Test03_quickSort {

	
	public static void main(String[] args) {
		int [] arr = {3,2,4,6,9,1,8,7,5};
	
		quick(arr,0,arr.length-1);
		
		System.out.println(Arrays.toString(arr));
	}

	//병합정렬 ( O(n log n ) ) 
	public static void quick(int[] arr, int begin, int end) {
		
		if(begin < end ) {
			int p = begin;
			int i = begin+1;
			int j = end;
	
			while(i <= j) {
				while(arr[i] <= arr[p]) i++;
				while(arr[j] > arr[p]) j--;
				
				if(i<j) Sort.swap(arr,i,j);
			}
			Sort.swap(arr,p,j);
			quick(arr,begin,j-1);
			quick(arr,j+1,end);
		}
	}
}
