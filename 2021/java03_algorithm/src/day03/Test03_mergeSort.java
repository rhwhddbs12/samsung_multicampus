package day03;

import java.util.Arrays;

public class Test03_mergeSort {

	static int[] temp = null; //merge 할때 필요한 기억공간
	public static void main(String[] args) {
		int [] arr = {69,10,30,2,16,8,31,22};
		temp = new int[arr.length];
		merge(arr,0,arr.length-1);
		
		System.out.println(Arrays.toString(arr));
	}

	//병합정렬 ( O(n log n ) ) 
	
	public static void merge(int[] arr, int begin, int end) {
		if(begin>=end) return;
		
		//분할 쪼개는 작업
		int mid = (begin + end )/2;
		merge(arr,begin,mid);
		merge(arr,mid+1,end);
		
		//정복 머지하는 작업
		//temporary한 기억공간 하나가 필요 **
		
		for(int i=begin ;i<=end;i++) {
			temp[i] = arr[i];
			
		}
		int k = begin,j = mid+1,i = begin;
		
		while(i <= mid && j <=end) {
			arr[k++] = temp[i]<temp[j] ? temp[i++]:temp[j++];

		}
		while(i<=mid) arr[k++] = temp[i++];
		while(j<=end) arr[k++] = temp[j++];

	}
}
