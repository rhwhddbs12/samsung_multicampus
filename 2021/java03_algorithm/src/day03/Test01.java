package day03;

public class Test01 {
	static int[] arr = {4,6,8,9,23,56,77,89,99,122,234,543,666};
	static int N = arr.length;
	public static void main(String[] args) {
		int key = 99;
		int idx = binSearch(arr,0,N-1,key);
		System.out.printf("%d ==> %d %n",key,idx);

	}
	// 이진검색 시간복잡도 ( O(log n) )
	// 전제조건:배열일 정렬되어 있어야한다.
		public static int binSearch(int[] arr,int start,int end,int key) {
			if(start > end) return -1; // 함수 종료 시점(무한루프 방지)
			
				int center = (start + end) / 2;
				// 정답을 찾은경우
				if( arr[center]== key ) return center;
				// 센터 오른쪽으로 쭉 값을 찾는다.
				if(arr[center] < key) return binSearch(arr,center+1,end,key);
				// 센터 왼쪽으로 쭉 값을 찾는다.
				else return binSearch(arr,start,center -1,key);
			
		
		}
}
