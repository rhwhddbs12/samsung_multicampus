package kr.co.java.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import kr.co.java.dao.MemberDAO;
import kr.co.java.dto.MemberDTO;

@WebServlet("/MemberJoin")
public class MemberjoinServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
   
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		request.setCharacterEncoding("utf-8"); //한글 입력깨질때 !
		//db저장
		MemberDAO dao = new MemberDAO();
		MemberDTO member = new MemberDTO();
		member.setId(request.getParameter("id"));
		member.setName(request.getParameter("name"));
		member.setPassword(request.getParameter("pw"));
		member.setEmail(request.getParameter("email"));
		//db저장 성공여부에 따른 결과를 보여주고싶었다.
		//dao.addMember(member);
		
		//포워딩 코드
		boolean resultFlag = dao.addMember(member);
		
		if(resultFlag) {
			response.sendRedirect("memberList");
		}else {
			response.sendRedirect("memberJoinForm.html");
		}
//		request.setAttribute("resultFlag", resultFlag);
//		RequestDispatcher rd = request.getRequestDispatcher("memberJoin.jsp");
//		rd.forward(request, response);
//		
//		RequestDispatcher rd = null;
//		if(resultFlag) {
//			rd = request.getRequestDispatcher("memberJoinOk.jsp");
//		}
//			rd = request.getRequestDispatcher("memberJoinfail.jsp");
//			
//		rd.forward(request, response);
	}

}
