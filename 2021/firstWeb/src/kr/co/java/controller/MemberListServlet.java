package kr.co.java.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.websocket.Session;

import kr.co.java.dao.MemberDAO;
import kr.co.java.dto.MemberDTO;

@WebServlet("/memberList")
public class MemberListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
   
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		boolean loginFlag = false;
		String id = null;
//		Cookie[] cookies = request.getCookies();
//		if(cookies!=null) {
//			for(Cookie cookie: cookies) {
//				if("loginOK".equals(cookie.getName())) {
//					loginFlag = true;
//					id = cookie.getValue(); 
//					break;
//				}
//			}
//		}
		HttpSession session = request.getSession(); 
		MemberDTO member = (MemberDTO)session.getAttribute("loginOK");
		if(member != null) {
			loginFlag = true;
			id = member.getId();
		}
		if(loginFlag) {
			//로그인이 된 사용자라면 ..리스트를 보여준다.
			
			//1.DAO에게 memberList = dao.getMemberList();
			MemberDAO dao = new MemberDAO();
			List<MemberDTO> memberList = dao.getMemberList();
			//2. 얻어온 memberList를 request객체에게 맡긴다.
			request.setAttribute("memberList", memberList); //고종윤,자켓("객체명",객체)
			request.setAttribute("id", id);
			//3. memberList.jsp로 포워딩
	//		response.sendRedirect("memberList.jsp");
			RequestDispatcher rd = request.getRequestDispatcher("memberList.jsp");
			rd.forward(request, response); // URl이 바뀌지않는다. 
		}else {
			//로그인 하지 않은 사용자에게는 리스트를 보여주지 않는다.
			response.sendRedirect("loginForm.jsp");
		}
	}

}
