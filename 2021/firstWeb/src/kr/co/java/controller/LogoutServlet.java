package kr.co.java.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class LogoutServlet
 */
@WebServlet("/LogoutServlet")
public class LogoutServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
 
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//쿠키를 사여ㅛㅇ 했을
		//		Cookie cookie = new Cookie("loginOK","");
//		cookie.setPath("/");
//		cookie.setMaxAge(0); // -1은 브라우저가 유지되는 동안.
//		response.addCookie(cookie);
//		
		
		HttpSession session = request.getSession();
		session.removeAttribute("loginOK");
		
		response.sendRedirect("memberList");
	}

}
