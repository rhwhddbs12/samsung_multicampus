package kr.co.java;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

/**
 * Application Lifecycle Listener implementation class TestListener
 *
 */
@WebListener
public class TestListener implements ServletContextListener {

    
    public TestListener() {
        System.out.println("테스트 리스너 생성자 실행 ");
    }

    public void contextDestroyed(ServletContextEvent arg0)  { 
         System.out.println("테스트 리스너 디스트로이 실행");
    }

    public void contextInitialized(ServletContextEvent arg0)  {
    	//와스가 처음 실행될때 초기화할 부분이라던지 그런부분들을 적절하게 이코드내에서 변경
         System.out.println("테스트 리스너 이니셜라이즈 실행");
    }
	
}
