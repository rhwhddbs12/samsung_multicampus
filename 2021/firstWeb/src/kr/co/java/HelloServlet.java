package kr.co.java;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class HelloServlet
 */
@WebServlet("/hi")
public class HelloServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	public HelloServlet() {
		System.out.println("Helloservlet 생성자 실행");
	}
	@Override
	public void init() throws ServletException {
		System.out.println("Helloservlet init 실행");
		// TODO Auto-generated method stub
		
		super.init();
	}
	
	@Override
	public void destroy() {
		System.out.println("Helloservlet destroy 실행");
		super.destroy();
	}

	

   //JSP가 나오기전 서블릿을 통해 동적인html을 만들때 이렇게사용했다 !
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println("Helloservlet service 실행");
		System.out.println(request.getParameter("name"));
		System.out.println("짹짹!!");
		response.setContentType("text/html;charset=utf-8");
		PrintWriter out = response.getWriter();
		out.print("<html><head><title>hello servlet!</title>");
		out.print("<body>");
		out.print("<h1> hahaha </h1>");
		;
		for(int i = 0;i<10;i++) {
			out.print("안녕!!"+request.getParameter("name")+"<br>");
		}
		out.print("</body></html>");
		out.close();
	}

	

}
