package kr.co.java.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.Cookie;

import kr.co.java.common.DBUtill;
import kr.co.java.dto.MemberDTO;

public class MemberDAO {
	//입력
	public boolean addMember(MemberDTO member) {
		boolean resultFlag = false;
		//1. 필요한 객체들을 선언..
		Connection conn = null;
		PreparedStatement ps = null;
		try {
		//3. DB접속
			conn = DBUtill.getConnection();
		//4. 쿼리문 준비
			String sql = "insert into MEMBERS(id,name,password,email,join_date) values(?,?,?,?,sysdate)";
			ps = conn.prepareStatement(sql);
			ps.setString(1, member.getId());
			ps.setString(2, member.getName());
			ps.setString(3, member.getPassword());
			ps.setString(4, member.getEmail());
		//5. 실행.
			int count = ps.executeUpdate();
			if(count ==1) resultFlag = true;
		}catch(Exception e) {
			e.printStackTrace();
		}finally {
			//2. 선언한 객체 닫기.
			DBUtill.close(conn,ps);
		}
		return resultFlag;
	}
	//멤버조회 
	public MemberDTO findMember(String id) {
		MemberDTO member = null;
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
		//3. DB접속
			conn = DBUtill.getConnection();
		//4. 쿼리문 준비
			String sql = "select id,name,password,email,join_date from MEMBERS where id =?";
			ps = conn.prepareStatement(sql);
			ps.setString(1, id);
		//5. 실행.
			rs = ps.executeQuery();
			while(rs.next()) {
				member = new MemberDTO();
				member.setId(rs.getString("id"));
				member.setName(rs.getString(2));
				member.setPassword(rs.getString(3));
				member.setEmail(rs.getString(4));
				member.setJoindate(rs.getString(5));
			}
		}catch(Exception e) {
			e.printStackTrace();
		}finally {
			//2. 선언한 객체 닫기.
			DBUtill.close(conn,ps,rs);
		}
		return member;
	}
	//리스트 조회
	public List<MemberDTO> getMemberList() {
		List<MemberDTO> memberList = new ArrayList<MemberDTO>();
		//1. 필요한 객체들을 선언..
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		MemberDTO dto;
		
		try {
		//3. DB접속
			conn = DBUtill.getConnection();
		//4. 쿼리문 준비
			String sql = "select id,name,password,email,join_date from MEMBERS";
			ps = conn.prepareStatement(sql);
			
			rs = ps.executeQuery();   // select
			//결과값 핸들링
			while(rs.next()) {
				dto = new MemberDTO();
				dto.setId(rs.getString("id"));
				dto.setName( rs.getString("name"));
				dto.setPassword( rs.getString("password"));
				dto.setEmail(rs.getString("email"));
				dto.setJoindate(rs.getString("join_date"));
				memberList.add(dto);
			}
		//5. 실행.
			ps.executeUpdate();
			
		}catch(Exception e) {
			e.printStackTrace();
		}finally {
			//2. 선언한 객체 닫기.
			DBUtill.close(conn,ps,rs);
		}
		return memberList;
	}
	
	//멤버 삭제 
	public int deleteMember(String id) {
		int resultCount =0;
		Connection conn = null;
		PreparedStatement ps= null;
		try {
			conn = DBUtill.getConnection();
			ps= conn.prepareStatement("delete from members where id= ?");
			ps.setString(1, id);
			
			resultCount = ps.executeUpdate();
		}catch(Exception e) {
			e.printStackTrace();
		}finally {
			DBUtill.close(conn,ps);
		}
		
		return resultCount;
	}
	//수정
	public void updateMember(MemberDTO member) {
		Connection conn = null;
		PreparedStatement ps= null;
		try {
			conn = DBUtill.getConnection();
			String sql = "Update members set name =?,password=?,email=? where id=? ";
			ps= conn.prepareStatement(sql);
			ps.setString(1, member.getName());
			ps.setString(2, member.getPassword());
			ps.setString(3, member.getEmail());
			ps.setString(4, member.getId());
			ps.executeUpdate();
		}catch(Exception e) {
			e.printStackTrace();
		}finally {
			DBUtill.close(conn,ps);
		}
	}
	
	public static void main(String[] args) {
		MemberDAO dao = new MemberDAO();
		MemberDTO member = new MemberDTO();
		List<MemberDTO> memberList = dao.getMemberList();
//		member.setId("ko233");
//		member.setName("고종윤");
//		member.setPassword("12345");
//		member.setEmail("rhwhddbs123@naver.com");
//		System.out.println(dao.addMember(member));
//		for(MemberDTO memberDTO : memberList) {
//			System.out.println(memberDTO);
//		}
//		System.out.println(dao.getMemberList());
		
		System.out.println(dao.deleteMember("power"));
	}

}
