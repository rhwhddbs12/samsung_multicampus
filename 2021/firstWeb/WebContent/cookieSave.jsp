<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%
	//쿠키저장
	String cookieName = request.getParameter("cookieName");
	String cookieValue = request.getParameter("cookieValue");
	
	//쿠키생성
	
	Cookie cookie = new Cookie(cookieName,cookieValue);
	cookie.setPath("/");
	cookie.setMaxAge(-1); // 초단위로 쿠키 유지시간을 줄 수 있음..
	
	//쿠키를 응답결과에 포함시켜야 함. 반드시!
	response.addCookie(cookie);
	
	response.sendRedirect("cookieView.jsp");

%>