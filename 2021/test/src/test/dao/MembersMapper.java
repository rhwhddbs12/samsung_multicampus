package test.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import test.dto.Member;

@Mapper
public interface MembersMapper {
	public Member getMember(String id);
	public int addMember(Member member);
	public List<Member> getMembers();
}
