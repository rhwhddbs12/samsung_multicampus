package Ioc_exam_tv;

public class TVFactory {
	public static TV getTV(String tvName) {
		TV tv = null;
		if("G".contentEquals(tvName)) {
			tv= new GoTV();
		}else if("K".equalsIgnoreCase(tvName)) {
			tv = new KimTV(); 
		}
		return tv;
	}
}
