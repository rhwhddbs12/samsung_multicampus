package user;

import Ioc_exam_tv.GoTV;
import Ioc_exam_tv.KimTV;
import Ioc_exam_tv.STV;
import Ioc_exam_tv.TV;
import Ioc_exam_tv.TVFactory;

public class TVUser {
	
	public static void main(String[] args) {
//		STV tv = new STV();
//		tv.turnOn();
//		tv.turnOff();
//		tv.volumeUp();
//		tv.volumeDown();
		
//		TV tv = new /*GoTV();*/ KimTV();  TV를 바꾸면 객체를 다시 생성해야함 ! 
		
		TV tv = TVFactory.getTV(args[0]);
		
		tv.turnOn();
		tv.turnOff();
		tv.volumeUp();
		tv.volumeDown();

	}
}
