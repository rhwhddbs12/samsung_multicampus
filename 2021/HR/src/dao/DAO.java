package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import common.DBUtill;
import dto.DTO;



public class DAO {
	
	public boolean addMemer(DTO dto) {
		boolean resultFlag = false;
		//1. 필요한 객체들을 선언..
		Connection conn = null;
		PreparedStatement ps = null;
		try {
		//3. DB접속
			conn = DBUtill.getConnection();
			String sql = "insert into emp(empid,ename) values(?,?)";
			ps = conn.prepareStatement(sql);
			ps.setString(1, dto.getEmpid());
			ps.setString(2, dto.getEname());
			
			
		//5. 실행.
			int count = ps.executeUpdate();
			if(count ==1) resultFlag = true;
		}catch(Exception e) {
			e.printStackTrace();
		}finally {
			DBUtill.close(conn);
		}
		
		
		return resultFlag;
	}
	public List<DTO> memberList(){
		List<DTO> memberList = new ArrayList<DTO>();
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		DTO dto = null;
		try {
			conn = DBUtill.getConnection();
			String sql = "select empid,ename from emp";
			ps = conn.prepareStatement(sql);
			rs = ps.executeQuery();   // select
			
			while(rs.next()) {
				dto = new DTO();
				dto.setEmpid(rs.getString("empid"));
				dto.setEname(rs.getString("ename"));
				memberList.add(dto);
			}
		}catch(Exception e) {
			e.printStackTrace();
		}finally {
			DBUtill.close(conn, ps, rs);
		}
		
		return memberList;
	}
	//멤버 삭제 
		public int deleteMember(String id) {
			int resultCount =0;
			Connection conn = null;
			PreparedStatement ps= null;
			try {
				conn = DBUtill.getConnection();
				ps= conn.prepareStatement("delete from emp where empid= ?");
				ps.setString(1, id);
				
				resultCount = ps.executeUpdate();
			}catch(Exception e) {
				e.printStackTrace();
			}finally {
				DBUtill.close(conn,ps);
			}
			
			return resultCount;
		}
	public static void main(String[] args) {
		DAO dao = new DAO();
		DTO dto = new DTO();
		dto.setEmpid("ko233");
		dto.setEname("고종윤");
		System.out.println(dao.addMemer(dto));
	}
}
