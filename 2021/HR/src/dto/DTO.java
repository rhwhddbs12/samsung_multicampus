package dto;

public class DTO {
	private String empid;
	private String ename;
	public String getEmpid() {
		return empid;
	}
	public void setEmpid(String empid) {
		this.empid = empid;
	}
	public String getEname() {
		return ename;
	}
	public void setEname(String ename) {
		this.ename = ename;
	}
	@Override
	public String toString() {
		return "DTO [empid=" + empid + ", ename=" + ename + "]";
	}

	
}
