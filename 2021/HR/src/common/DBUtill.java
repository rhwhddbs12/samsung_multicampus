package common;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

//hr/employee table에서 사원 정보 등록, 사원리스트 보기, 사원 삭제 기능이 가능하도록
public class DBUtill {
	public static Connection getConnection() throws Exception{
		String driver = "oracle.jdbc.driver.OracleDriver";
		String url ="jdbc:oracle:thin:@127.0.0.1:55003:xe";
		String user = "hr";
		String pw = "1234";
		
		Connection conn = null;
		Class.forName(driver);
		conn = DriverManager.getConnection(url,user,pw);
		
		return conn;
	}
	public static void close(Connection conn, PreparedStatement ps ,ResultSet rs) {
		if(rs != null) {
			try {
				rs.close();
			}catch(SQLException e) {
				e.printStackTrace();
			}
		}
		close(conn,ps);
	}
	public static void close(Connection conn, PreparedStatement ps) {
		if(ps != null) {
			try {
				ps.close();
			}catch(SQLException e) {
				e.printStackTrace();
			}
		}
		close(conn);
	}
	public static void close(Connection conn) {
		if(conn != null) {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	public static void main(String[] args) throws Exception {
		try {
		System.out.println(DBUtill.getConnection());
		} catch(Exception e){
			e.printStackTrace();
		}
	}
}
