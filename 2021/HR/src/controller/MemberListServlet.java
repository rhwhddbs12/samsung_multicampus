package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.DAO;
import dto.DTO;


@WebServlet("/memberList")
public class MemberListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
   
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		//1.DAO에게 memberList = dao.getMemberList();
		DAO dao = new DAO();
		List<DTO> memberList = dao.memberList();
		//2. 얻어온 memberList를 request객체에게 맡긴다.
		request.setAttribute("memberList", memberList); //고종윤,자켓("객체명",객체)
		
		//3. memberList.jsp로 포워딩
//				response.sendRedirect("memberList.jsp");
		RequestDispatcher rd = request.getRequestDispatcher("memberList.jsp");
		rd.forward(request, response); // URl이 바뀌지않는다. 
	}

}
