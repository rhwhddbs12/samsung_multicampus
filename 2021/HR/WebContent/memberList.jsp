<%@page import="dto.DTO"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
<%
	List<DTO> memberList = (List<DTO>)request.getAttribute("memberList");
	System.out.println(memberList);
%>
<H1>회원목록보기!! </H1>
<table>
	<tr>
		<th>아이디</th>
		<th>이름</th>
		<th>비밀번호</th>
		<th>이메일</th>
		<th>가입일</th>
		<th>삭제</th>
	</tr>
	<% if(memberList != null){
	for(DTO dto:memberList){ %>
	<tr>
		<td><%=dto.getEmpid() %></td>
		<td><%=dto.getEname() %></td>
		<td><a href='memberDelete?id=<%=dto.getEmpid() %>'>삭제</a></td>
	</tr>
	
	<%} //end for
	}//end if %>
</table>
</body>
</html>