<%@page import="todo.dto.ToDo"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ToDo</title>
</head>
<body>

<H1>To Do List</H1>
<form action="add" method="post">
<input type="text" name="content" id="content">
<input type="submit" value="등록"> 
</form>

<table>
	<tr>
		<th>아이디</th>
		<th>내용</th>
		<th>상태</th>
	</tr>
	<c:forEach var="toDo" items="${list }" >	
	<tr>
		<td>${toDo.id}</td>
		<td>${toDo.content}</td>
		<c:set var="state" value= "${toDo.state}"/>
		<c:if test="${state eq '0'.charAt(0)}">
			<td><button onclick="location.href='update=${toDo.id}&${toDo.state}' ">미완료 </button></td>
		</c:if>
		
		<c:if test="${state eq '1'.charAt(0)}">
			<td><button onclick="location.href='update=${toDo.id}&${toDo.state}' ">완료 </button></td>
		</c:if>
		<td><button onclick="location.href='delete=${toDo.id}' ">삭제 </button></td>
	</tr>	
	</c:forEach>
</table>
<br>
<br>
</body>
</html>