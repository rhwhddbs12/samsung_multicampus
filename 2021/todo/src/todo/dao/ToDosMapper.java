package todo.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import todo.dto.ToDo;

@Mapper
public interface ToDosMapper {
	public List<ToDo> getToDos();
	public int addToDo(ToDo toDo);
	public void updateToDo(@Param("id")int id,@Param("state") char state);
	public void deleteToDo(int id);
}
