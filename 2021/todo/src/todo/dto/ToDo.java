package todo.dto;

public class ToDo {
	private int id;
	private String content;
	private char state;
	@Override
	public String toString() {
		return "ToDo [id=" + id + ", content=" + content + ", state=" + state + "]";
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public char getState() {
		return state;
	}
	public void setState(char state) {
		this.state = state;
	}
}
