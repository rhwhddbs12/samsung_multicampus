package todo.controller;


import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import todo.dto.ToDo;
import todo.service.ToDoService;

@Controller 
@RequestMapping(path = "/con" )
public class ToDoController {
	
	@Autowired
	private ToDoService toDoService;
	
	@GetMapping("/test")
	public String hello() {
		return "todo";
	}
	@GetMapping("/list")
	public String getToDos(ModelMap model) {
		model.addAttribute("list",toDoService.getToDos());
		return "todo";
	}
	@PostMapping("/add")
	public String addToDo(@ModelAttribute ToDo toDo, HttpServletRequest request) {
		System.out.println(toDo);
		toDoService.addTodo(toDo);
		return "redirect:list";
	}
	@GetMapping("/delete={id}")
	public String deleteToDo(@PathVariable(name="id") int id) {
		toDoService.deleteToDo(id);
		return "redirect:list";
	}
	@GetMapping("/update={id}&{state}")
	public String updateToDo(@PathVariable(name="id") int id,
							@PathVariable(name="state") char state) {
		if(state == '0') state ='1';
		else state ='0';
		toDoService.updateToDo(id,state);
		return "redirect:list";
	}
	
}
