package todo.config;

import javax.sql.DataSource;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.jdbc.datasource.SimpleDriverDataSource;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;


@Configuration
@ComponentScan(basePackages = {"todo.dao","todo.service"})
@Import({MyBatisConfig.class})  //myBatis클래스를 포함하게
@EnableTransactionManagement //이걸 사용해야 내부적으로 트랜잭션이 가능하다 . AOP를 사용함 
public class ApplicationConfig {
	@Bean   //디비변경시 DataSource 공통으로 사용되어 져야하기때문에 bean을 mybatis보다 applicationConfig에 등록하는 편이 유지보수에서훨씬좋다.
	public DataSource dataSource() {
		SimpleDriverDataSource dataSource = new SimpleDriverDataSource();
		//DB연결해서 커넥션객체 얻어올때 쓰던
		dataSource.setDriverClass(oracle.jdbc.driver.OracleDriver.class);
		dataSource.setUrl("jdbc:oracle:thin:@localhost:55003:xe");
		dataSource.setUsername("system");
		dataSource.setPassword("1234");
		return dataSource;
	}
	@Bean
	public PlatformTransactionManager transactionManager() {
		return new DataSourceTransactionManager(dataSource()); 
		// 원래는con.rollback con.commit 이런식으로 썻는데 여기서는 con-> dataSource 이기때문에 
	}
}
