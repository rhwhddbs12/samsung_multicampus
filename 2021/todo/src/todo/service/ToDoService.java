package todo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import todo.dao.ToDosMapper;
import todo.dto.ToDo;


@Service
public class ToDoService {
	@Autowired
	private ToDosMapper toDosMapper;
	
	@Transactional(readOnly= true)
	public List<ToDo> getToDos(){
		return toDosMapper.getToDos();
	}
	@Transactional(readOnly= false)
	public void addTodo(ToDo toDo) {
		toDosMapper.addToDo(toDo);
	}
	@Transactional(readOnly= false)
	public void deleteToDo(int id) {
		toDosMapper.deleteToDo(id);
	}
	@Transactional(readOnly= false)
	public void updateToDo(int id,char state) {
		toDosMapper.updateToDo(id,state);
	}
	
}
