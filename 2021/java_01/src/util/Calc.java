package util;

public class Calc {
	// 더하기 빼기 곱하기 나누기
	// ******* static한 자원은 new 안해도 메모리에 뜬다!
//------------- 메소드 오버로딩
	public static int add(int ... a) {      // (int...a,int c)-> x (int c, int...a)-> o
		int sum = 0;
		for(int data:a) {
			sum += data;
		}
		return sum;             //...은 배열로 처리하겠다 즉 a는배
	}
	public static int add(int a, int b) {
		return a+b;
	}
	public static int add(int a, int b, int c) {
		return a+b+c;
	}
	public static double add(double a, double b) {
		return a+b;
	}
	
//-----------------
	public static int sub(int a, int b) {
		return a-b;
	}
	public static int multi(int a, int b) {
		return a*b;
	}
	public static int div(int a, int b) {
		if(b!=0) return a/b;
		else return -1;
	}

}
