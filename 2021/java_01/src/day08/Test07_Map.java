package day08;

import java.util.HashMap;
import java.util.Map;

public class Test07_Map {
	public static void main(String[] args) {
		//map은 검색속도가 아주빠른 자료구조
		Map<String, String> map = new HashMap<String, String>();
		map.put("java01","1234");
		map.put("java02","2222");
		map.put("java03","1111");
		map.put("java07","2222");
		map.put("java09","9999");
		map.put("java011","1111");

		System.out.println(map);
		System.out.println(map.get("java01"));
		
		
	}

}
