package day08;

public class Test02 {

	public static void main(String[] args) {
		Employee emp1 = new Employee("홍길동",1234);
		System.out.println(emp1);

		Employee<String,Integer> emp2 = new Employee("고길동",1234);
		System.out.println(emp2);

		Employee<String,Double> emp3 = new Employee("박길동",1234.32);
		System.out.println(emp3);
		
	}

}

//P는 적어도 넘버의 자식이었으면 좋겠다. 
class Employee<T,P extends Number>{
	T name;
	P num;
	public Employee(T name, P num) {
		super();
		this.name = name;
		this.num = num;
	}
	public T getName() {
		return name;
	}
	public void setName(T name) {
		this.name = name;
	}
	public P getNum() {
		return num;
	}
	public void setNum(P num) {
		this.num = num;
	}
	@Override
	public String toString() {
		return "Employee [name=" + name + ", num=" + num + "]";
	}
	
	
}