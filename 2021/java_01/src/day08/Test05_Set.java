package day08;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.TreeSet;

public class Test05_Set {

	public static void main(String[] args) {
//		Set<String> set = new HashSet<String>();
		Set<String> set = new TreeSet<String>(); // Tree-> 자동 정렬
		set.add("홍길동");
		set.add("김길동");
		if(set.add("이길동")) {
			System.out.println("등록 O");
		}else{
			System.out.println("등록 X");			
		};
		set.add("박길동");
		System.out.println(set);  // set과 list의 차이는 중복차이
		// set:중복 불가 list:중복 가능
		// set:입력 순서x list: 입력 순서o
		Iterator<String> it = set.iterator();
		while(it.hasNext()) {
			String data = it.next();
			if(data.equals("홍길동")) it.remove();
//			System.out.println(data);
			}
		System.out.println(set);
		
	}

}
