package day07;

import java.util.Stack;

public class prob7_stack {
	public static void main(String args) {
		Stack<Integer> s = new Stack<Integer>();
		MyStack stack = new MyStack(10);
		if(stack.isEmpty()) {
			System.out.println("스택이 비어있음");
		}
		for(int i = 1; i <10; i++) {
			stack.push(i);
		}
		if(stack.isFull()) {
			System.out.println("스택이 가득참");
		}
		System.out.println("최상위 숫자:"+ stack.top());
		System.out.println("최상위에서 꺼낸 숫자: "+ stack.pop());
		System.out.println("최상위에서 꺼낸 숫자: "+ stack.pop());
		System.out.println("");
		System.out.println("== 스택 리스트 ==");
		for(int i = 1; i <=10;i++) {
			int num = stack.pop();
			if(num!= -1)
				System.out.println(num);
		}
		for(int i = 1; i <=100;i++) {
			stack.push(i);
		}
	}
}

class MyStack{
	int[] stack;
	int count = 0;
	public MyStack() {
		stack = new int [10];
	}
	public MyStack(int size) {
		stack = new int[size>0?size:10];// 삼항연산자로 간단하 if 쓰지않고.
	}
	public boolean isEmpty() {
		return count == 0 ? true:false;
	}
	public boolean isFull() {
		return count == stack.length ? true:false;
	}
	public void push(int i ) {
		if(isFull()) {
			System.out.println("스택이 가득 참=>  스택이 리 사이징 되었음.");
			int[] temp = new int[stack.length*2];
			System.arraycopy(stack,0,temp,0,stack.length);
		}else {
			stack[count++] = i;
		}
	}
	
	public int top() {
		return count==0?-1:stack[count-1];
	}
	public int pop() {
		int data = -1;
		
		if(isEmpty()) {
			data = stack[count-1];
			stack[count-1]= 0;
			count--;
		}
		return data;
	}
}
