package day04;

import java.util.Arrays;

public class Test01_Array {
	public static void main(String[] args) {
		//배열선언 -> 배열 생성 -> 배열 초기화 ( 배열은 무조건 주소)
		int[] score = null;
		score = new int [5]; //모든 배열 인덱스는 0으로 default
		System.out.println(Arrays.toString(score));

		score[0] = 3;
		score[1] = 28;
		score[2] = 20;
		score[3] = 300;
		score[4] = 40;
		
		
		
		int sum=0;
		for(int i = 0 ; i<score.length;i++) {
			sum += score[i];
		}
		System.out.println(sum);
		System.out.println("=========================");
		
		for(int data:score) {
			System.out.println(data);
		}
		int[] num = new int[10];
		System.out.println(Arrays.toString(num)); 
		
		for(int i =0;i<num.length;i++) {
			num[i] = (int)(Math.random()*100);
		}
		sum = 0;
		for(int data : num) {
			sum+= data;
			System.out.printf("%d,",data);
		}
		System.out.println(sum);
	}
}
