package day04;

public class Test05_doubleAry {

	public static void main(String[] args) {
		int[] num1 = {1,2,3,4,5};
		int[] num2 = {2,2,3,4,5};
		int[] num3 = {3,2,3,4,5};
		
		int[][] two = {num1,num2,num3};
		//이차원 배열.
		for(int row =0;row<two.length;row++){
			for(int col = 0;col<two[row].length;col++) {
				System.out.printf("%d ",two[row][col]);
				System.out.println(two[2][0]);
				
				
			}
			System.out.println();
			
		}
		
		int n=7;
		int[][] two1 = new int[n][n];
		two[2][3] = 99;
		
		System.out.println();
		
		
		int[][] two2 = {{},{90,40,88,99,89},{33,99,44,77,88,67,89},{99,88,100}};
		
		//반별 평균 점수
		int sum;
		for(int i =0;i<two2.length;i++) {
			sum=0;
			for(int j=0;j<two2[i].length;j++) {
				sum+= two2[i][j];
			}
			if(i!=0)
			System.out.printf("%d반 평균:%d %n",i,sum/two2[i].length);
		}
		
	}

}
