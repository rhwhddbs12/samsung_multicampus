package day04;

public class prob4 {
	
	//제출
	public static void main(String[] args) {
		//예제1===============================
		String msg ="yes";
		if(msg.equals("yes")) {
			System.out.println("true");
		}else {
			System.out.println("false");			
		}
		
		//예제2===============================
		
		int sum2 = 0;
		int x=1;
		while(sum2 < 100) {
			if(x%2==0) {
				sum2-=x;
				
			}else {
				sum2+=x;	
				
			}
			if(sum2>99) {
				System.out.printf("%d까지 더해야 함",x);
			}
			x++;
		}
		System.out.printf("sum2:%d %n",sum2);
		
		//예제3================================
		int[] arr = {10, 20, 30, 40, 50}; 
		int sum=0;
		for(int i =0; i<arr.length;i++ ) {
			sum+=arr[i];
		}
		
		System.out.println("sum="+sum);
		
		
		
		
		
		
		//제출==================================
		String rev = "";
		String[] strData  = { "Java Programming" , "JDBC", "Oracle10g", "JSP/Servlet" };
		for(int i=0; i<strData.length;i++) {
			//System.out.println(strData[i]);
			rev = "";
			for(int j = strData[i].length()-1; j> -1;j--) {
				rev += strData[i].charAt(j);
			}
			System.out.println(rev);
		}
	}

}


/*
Prob2. Prob2 클래스의 main() 에서
주어진 문자열 배열을 생성하여 
배열의 내용을 역순으로 출력하할 수 있도록 
main 메서드를 작성하세요. 

-	문자열 배열의 내용을 역순으로 출력하되 각 문자열 또한 역순으로 출력한다.
-	입력으로 주어진 문자열 배열의 예 :
  { "Java Programming" , "JDBC", "Oracle10g", "JSP/Servlet" }

처리 결과의 예 : 아래 참고.
gnimmargorP avaJ
CBDJ
g01elcarO
telvreS/PSJ

public class Prob2 {
	public static void main(String[] args) {
		String[] strData  = { "Java Programming" , "JDBC", "Oracle10g", "JSP/Servlet" };	
	}
}
*/