package day04;

public class Emp {
	//필드
	//private :해당 클래스에서만 접근가능
	//public: 어디서든 접근가능
	
		private String name;
		private String dept;
		private int age;     
		
		
		//static 영역에서는 this 키워드사용 불가.
		//static은 공유 느낌
		static double pi = 3.14;
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public String getDept() {
			return dept;
		}
		public void setDept(String dept) {
			this.dept = dept;
		}
		public void setAge(int age){
			if(age>0)
			this.age = age;
		}
		public int getAge(){
			
			return age;
		}
		//메서드
		public void print() {
			System.out.printf("사원명:%s 근무부서:%s 나이:%s ",this.name,this.dept,this.age);
		}
		
}
