package day04;

import java.util.Arrays;

public class Test02 {

	public static void main(String[] args) {
//		int[] num = new int[5];
		int[] num = {1,23,24,52,76,10};
		System.out.println(Arrays.toString(num));
		
		String[] names = new String[] {"홍길동","고길동","김길동","이이"};
		System.out.println(Arrays.toString(names)); 
		
		// num 배열에서 50 번찾기
		for(int i = 0; i<num.length;i++){
			if(num[i]==50) {				
				System.out.printf("%d 위치 O",i);
				break;
			}
			System.out.println("x");
			
		}
		// names 배열에서 김길동 찾
		for(int i=0;i<names.length;i++) {
			if(names[i].equals("김길동")) {
				System.out.printf("%d 위치 O",i);
				break;
			}
		}
		// names 배열에서 이름이 두글자인 사람만 출력하
		for(int i=0;i<names.length;i++) {
			if(names[i].length() == 2) {
				System.out.printf(names[i]);
				break;
			}
		}
		//배열의 사이즈는 .length
		//문자열의 사이즈는 .length()
		for(String data : names) {
			System.out.printf("%s , %c** %n",data,data.charAt(0));
		}
		
		char[] ch = new char[26];
		char[] ch2 = "hello java".toCharArray();  //스트링 toCharArray()로 뽑
		System.out.println(ch2);
		boolean[] flag = new boolean[5];
	}

}

