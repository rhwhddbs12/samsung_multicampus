package day04;

public class Test06_class {

	public static void main(String[] args) {
		Emp emp1 = new Emp();
		emp1.setName("홍길동");
		emp1.setAge(22);
		emp1.setDept("개발");
		//emp1.print("홍길동","개발",33);
		Emp emp2 = new Emp();  
		emp2.setName("길동");
		emp2.setAge(32);
		emp2.setDept("영업");
		//emp2.print();
		Emp emp3 = new Emp();  
		emp3.setName("이이");
		emp3.setAge(27);
		emp3.setDept("인사");
		Emp[] employees1 = new Emp[100];
		Emp[] employees = {emp1,emp2,emp3,null,null,null,null};
		
		//employees 에서영업부에 근무하는 사원 목록
		for(int i=0;i<employees.length;i++) {
			if(employees[i] != null && employees[i].getDept()!=null 
									&&employees[i].getDept().equals("영업")) {
				employees[i].print();
//			System.out.printf("사원명:%s 부서명:%s 나이:%s %n",employees[i].name,employees[i].dept,employees[i].age);
			}
		}
		
	}

}
