package day04;

public class Test03_arycopy_split {

	public static void main(String[] args) {
		int [] num = null;
		num = new int[] {66,77,33,55,100};
		//위 아래 차이 위에라인 분리하면 new int 해줘야함 배열표시  뒤 상관없음.
		int[] num1 = {12,123,12,41,123};
		int num2[] = num1;
//		int[] num3 = num2;
		int[] num3 = num2.clone();  //이렇게하면num1메모리를 공유하지않는다.
		
		num1[0]= 11;
		if(5<num1.length &&5>=0) num1[5] =11;
		num2[0]= 22;
		num3[0]= 33;

		num1 =null;
		num2 =null;
		
		System.out.println("=========================");
		int[] num4 = new int[] {12,123,12,41,123};
		int[] num5 = new int[num4.length*2];
		
		System.arraycopy(num4, 0, num5, num4.length, num4.length);
		//(복사할 배열,시작 인덱스,담을 배열, 시작인데스, 담을배열의 길이)
		
		// 스트링 배열이 된다.
	String[] data = "고길동/99/77/100".split("/");
	double sum =0;
	for(int i=1;i<data.length;i++) {
		sum+= Double.parseDouble(data[i].trim());
		System.out.println(data[i]);			
	}
	System.out.printf("%s:%.2f %n",data[0],sum/3);
	
	
	}

}
