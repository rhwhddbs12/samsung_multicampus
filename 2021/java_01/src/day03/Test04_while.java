package day03;

import javax.swing.JOptionPane;

public class Test04_while {

	public static void main(String[] args) {
		int num = 99;
		
		for(int i=0;i<10;i++) {
			System.out.println();
		}
		for(int count=9;count>0;count--) {
			System.out.println();
		}
		
		int i = 0;
		while(i<5) {
			System.out.println("while loop");
			i++;
		}
		String msg = "";
		boolean flag = false;
		while(!flag) {
			System.out.println("-------------");
			msg = JOptionPane.showInputDialog("종료를 원하시면 quit");
			System.out.println("입력값:"+msg);
			if(msg.equalsIgnoreCase("quit")) flag = !flag;
		}
		
		i=9;
		do {
			System.out.println("whileloof");
			i++;
		}while(i<5);
		
		
		
		
	}

}
