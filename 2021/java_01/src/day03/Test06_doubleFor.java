package day03;

public class Test06_doubleFor {

	public static void main(String[] args) {
		
		OUT:for(int i=1;i<10;i++) {
			System.out.println("");
			for(int j=2;j<10;j++) {
				if(j==5)break; //블럭을 빠져나
				if(j==5)continue; //블럭처음으로 돌아감 재진입 
				if(j==5)break OUT; //블락의 지정된 위치로 
				System.out.printf("%d * %d = %d|",j,i,i*j);
			}
		}
		
	}

}
