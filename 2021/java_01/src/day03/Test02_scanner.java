package day03;

import java.util.Scanner;
public class Test02_scanner {

	public static void main(String[] args) {
		System.out.println("성적처리 APP START");
		
		String name = null;
		int kor = 0;
		int math = 0;
		// 스캐너 객체 생성 
		Scanner scanner = new Scanner(System.in);
		System.out.println("계속 진행:Yes입력 / 끝내기: no를 입력 ");
		String msg = scanner.nextLine();
		if(!msg.equalsIgnoreCase("Yes")) {
			if(scanner != null) scanner.close();//스캐너 자원 반납.
			scanner = null; //null을쓰면 주소를 잃어버리게됨 그전에 자원을 반납해야함. 
			System.out.println("종료합니다");
			
			//가비지의 대상이 되게 null표시 (*** null로인해 가비지의 대상이되는데 자원이 언제 반납될지는 모름)
			
			return;
		}
		
		
		System.out.println("학생 이름을 입력하세요.");
		name = scanner.nextLine();
		System.out.println("국어 점수를 입력하세요.");
		kor = Integer.parseInt(scanner.nextLine());
		System.out.println("수학 점수를 입력하세요.");
		math = scanner.nextInt();
		scanner.nextLine();  //엔터가 남은걸 해결 
		
		double avg = (kor+math) / 2;
		String error = avg >=0 && avg <=100 ? "":"error: 점수 확인 필요";
		//등급처리
		char grade = 'F'; //"",''
		if (avg>=90 && avg <=100) {
			grade ='A';
		}else if(avg>=80 && avg <=89) {
			grade='B';
		}else if(avg>=70 && avg <=79) {
			grade='C';
		}else if(avg>=60 && avg <=69) {
			grade='D';
		}else {
			grade='F';
		}
		
		// 삼항연산자로 변환 
		
//		char test = ' '; //"",' '
//		//switch()byte/short/int/char 만 올수 있음 (인풋타입으로 프로모션가능한 타입)+string까지 
//		switch( test ) {
//		case '+':
//			System.out.println("+");
//			break;
//			
//		case '-':
//			System.out.println("-");
//			break;
//			
//		case '*':
//			System.out.println("*");
//			break;
//			
//		case '/':
//			System.out.println("/");
//			break;
//		default:
//			System.out.println("입력오류");
//			
//		}
		
		char gradeS= ' ';
		switch((int)(avg)/10) {
		case 10:
		case 9:
			gradeS='A';
			break;
		case 8:
			gradeS='B';
			break;
		case 7:
			gradeS='C';
			break;
		case 6:
			gradeS='D';
			break;
		default:
			gradeS='F';
		}
		
		System.out.printf("이름: %s, 국어: %d 수학 %d 평균: %.2f 등급: %c %n",name,kor,math,avg,gradeS);
		System.out.println("성적처리 APP END");
		if(scanner!=null)
			scanner.close();
			scanner = null;
		return;

	}

}
