package day03;

public class Test07_String {

	public static void main(String[] args) {
		String msg = "hello"+","+"java"+","+"Spring";//스트링 +연산 아주 안좋은 코딩 하지마 
		System.out.println(msg);
		
		//스티링 빌더객체 (위에꺼보다 성능이 훨씬좋음)   (빌더가 요즘,버퍼는 옛날꺼지만 기능은같다)
		StringBuilder sb = new StringBuilder();
//		StringBuffer sb = new StringBuffer();
		sb.append("hello");
		sb.append(",");
		sb.append("java");
		sb.append(1);
		sb.append(",");
		sb.append("Spring");   //스트링이아닌 스트링 빌더타입 (문자,정수,불리언 다 가능)
		
		String result = sb.toString(); // 스트링 빌더타입을 스트링으로 컨버팅
		System.out.println(result);
	}

}
