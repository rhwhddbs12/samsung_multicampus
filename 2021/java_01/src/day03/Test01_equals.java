package day03;

import java.util.Date;

public class Test01_equals {

	public static void main(String[] args) {

		System.out.println(7==8);
		System.out.println("ABC".equals("ABC")); // 스트링은 비교할때 .equals() 참조형이
		System.out.println("java".equals("sql")); 
		System.out.println("sql".equals("java")); 
		System.out.println("Hello".equals("HELLO"));
		System.out.println("Hello".equalsIgnoreCase("HELLO"));
		
		String s1 = "";
		String s2 = "";
		System.out.println(s1.equals(s2));
		System.out.println(s2.equals(s1));
		System.out.println(s2.equalsIgnoreCase(s1));
		System.out.println(s1.equalsIgnoreCase(s2));
		
		int num = 99;
		Date d1 = new Date();
		String msg1 = new String("~~~~");
		String msg2 = "java";
		new String("java"); //템포러리 객체 힙영역에 저장되지만 레퍼런싱하진 않는다.
		d1=null;
		msg2 = msg1;
		
	}

}

/*
 * 가비지 컬렉터:메모리 관리 
 * 스택-힙-스태틱-코드표
 * 스택:로컬변수(초기화안되면 사용 불가)가 들어감 
 * 힙:가비지 컬렉터가 관리 객체가 여기에 저장됨(초기화 안해도 디폴트 자동저장) 쓰지않으면 가비지가 정리 
 * 스태틱:java.lang , Math, main함수 등이 스태틱에 저장되어 관리 메모리에 올라
 * 코드표:string msg2 = "java"는 여기에 저장  여기한번 올라가면 지워지지않
 * */

