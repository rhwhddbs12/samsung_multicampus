package day05;

import java.util.Calendar;

public class Test05_singleton {
	public static void main(String[] args) {
//		Singleton s1 = new Singleton();
//		System.out.println(s1);
//		Singleton s2 = new Singleton();
//		System.out.println(s2);
//		Singleton s3 = new Singleton();
//		System.out.println(s3);
//		Singleton s4 = new Singleton();
//		System.out.println(s4);
// new 연산자를 썻기때문에 제각각 다른 메모리 힙영역을 할당받게된
//============================================================
		
// Singleton기법을 사용하여 (하나의 메모리 영역을 가진)객체하나를 공유해서 쓸 수 있다.
		Singleton s1 = Singleton.getInstance();
		System.out.println(s1);
		Singleton s2 = Singleton.getInstance();
		System.out.println(s2);
		Singleton s3 = Singleton.getInstance();
		System.out.println(s3);
		Singleton s4 = Singleton.getInstance();
		System.out.println(s4);
		System.out.println("=================================================");
		
		//Calendar는 static한 자원이다.
		Calendar cal = Calendar.getInstance();
		
		
		new String();
		new String("ABC");
		new String("ABCDEFG".toCharArray());
		new String("ABCDEFG".toCharArray(),2,5);

		
		
	}

}

class Singleton{
	static Singleton s = new Singleton();
	private Singleton(){}
	
	//static 처리르 해야 외부에서 처리 가능하다. 주소 없어도 쓸 수 있다 static 자원이기 떄문!
	public static Singleton getInstance() {
		
		return s;
	}
}