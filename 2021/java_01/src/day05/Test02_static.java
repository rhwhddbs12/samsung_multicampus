package day05;

import util.Calc;

public class Test02_static {

	public static void main(String[] args) {
//		Calc c1 = new Calc();
//		int a= c1.add(123, 1234);
//		int b= c1.sub(9, 11);
		//new를 사용하여 메모리 주소를 할당받아 사용하는 방식
		
	
		//****** static은 이미 메모리에 있기때문에 new하여 주소를 할당받지않아도 클래스 네임으로 접근이 가능하다!
		System.out.println(Calc.add(13, 14));
		System.out.println(Calc.add(13, 34,13,13,4,14,14,14,23));
		System.out.println(Calc.add(1));
		System.out.println(Calc.add(3.2, 1.3));
		System.out.println(Calc.add(123, 1234));
		int a2= Calc.add(123, 1234,123);		
		double a3 = Calc.add(5.5,6.2);
		int b= Calc.sub(9, 11);
		int c = Calc.add(2);
		
		int res = Calc.multi(2, 2);
		int res2 = Calc.multi(2+7, Calc.add(9, 2));

	}

}


//***** 메모리 영역
//** new ->힙영역 (this사용가능)
//** static ->스태틱 영역 (this사용불가) 공유해도 되는경우에 사용 
