package day05;

import day04.Emp;

public class Test01 {

	public static void main(String[] args) {
		Emp emp1 = new Emp();
		emp1.setName("홍길동");
		emp1.setAge(22);
		emp1.setDept("개발");
		emp1.print();
		
//		Emp emp2 = null;   <-주소가 널이다
//		System.out.println(emp2.getDept());   <- 주소가 없어서 못쓴다. 인스턴스 자원(꼭 메모리 주소가 필요해) 
	
		Emp emp2 = null;
		if (emp2!=null)
		System.out.println(emp2.getDept()); //인스턴스 자원(new하여 메모리 할당 받아야한다 반드시)
		System.out.println(Math.random()); //static 자원(이미 메모리가 떠잇음)
	}

}
