package day05;

public class Test03_creater {
	public static void main(String[] args) {
		Emp emp1 = new Emp();
		emp1.print();

		
		Emp emp2 = new Emp("홍길동","기술",28);
		emp2.print();

		Emp emp3 = new Emp("고길동","영업",28);
		emp3.print();

		Emp emp4 = new Emp("이길동",29);
		emp4.print();
		
		
	}
}
