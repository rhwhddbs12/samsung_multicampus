package day05;
/**
 * 
 * @author gojy
 *
 */
public class Emp {
	//필드
	//private :해당 클래스에서만 접근가능
	//public: 어디서든 접근가능
	
		private String name;
		private String dept;
		private int age;     
		
		
		public Emp() {	}
		//source-> generationg constructer using field 생성자 생성
		public Emp(String name, String dept, int age){
			this.name = name;
			this.dept = dept;
			this.age = age;
			//System.out.println();
			
		}
		public Emp(String name,int age){
			this(name,null,age);    //생성자들끼리도 호출이가능함 // this() 첫번째 라인 위치 만 허용
//			this.name = name;
//			this.age = age;
			//System.out.println();
			
		}
		
		//static 영역에서는 this 키워드사용 불가.
		//static은 공유 느낌
		static double pi = 3.14;
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public String getDept() {
			return dept;
		}
		public void setDept(String dept) {
			this.dept = dept;
		}
		public void setAge(int age){
			if(age>0)
			this.age = age;
		}
		public int getAge(){
			
			return age;
		}
		
		/**
		 *  @param age 양수만 가능
		 */
		
		
		
		//출력 메서드
		public void print() {
			System.out.printf("사원명:%s 근무부서:%s 나이:%s %n",this.name,this.dept,this.age);
		}
		
}
