package day06_me;

public class Dog extends Animal{
	
	String kind = "강아지 종류";
	String name;
	
	
	
	public Dog() {
		super();
		//super.kind= "강아지";
	}

	public Dog(String kind, String name) {
		super();
		super.kind= "강아지";
		this.kind = kind;
		this.name = name;
	}

	public void print() {
		System.out.printf("Dog[ %s : %s : %s ]%n",super.kind,this.kind,name);
	}
	
	public static void main(String[] args) {
		Dog d1 = new Dog();
		d1.print();
		Dog d = new Dog("시츄","캐리");
		System.out.println(d.kind);
		System.out.println(d.name);
		d.breath();
		d.print();
		
	}
}


//static 안에서는this,super못쓴다 this랑 super는 힙 메모리안에서만 사용 가능하다.