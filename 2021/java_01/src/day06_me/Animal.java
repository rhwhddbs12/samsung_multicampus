package day06_me;

public class Animal {
	String kind = "동물의 종류";
		
	public Animal() {
	}
	public Animal(String kind) {
		this.kind = kind;
	}
	
	public void breath() {
		System.out.println("폐로 숨쉬기");
	}
	
	public static void main(String[] args) {
		Animal a = new Animal("강아지");
		System.out.println(a.kind);
		a.breath();
	}
}
