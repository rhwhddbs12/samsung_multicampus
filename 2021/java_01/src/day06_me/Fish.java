package day06_me;

public class Fish extends Animal {
	
	String name;
	
	public Fish() {
		
	}//super() 가 자동으로 생략되어있다 .
	
	public Fish(String name) {
		super("물고기");
		this.name = name;
	}

	public void print() {
		System.out.printf("Fish[ %s : %s ]%n",kind,name);
	}
	
	public static void main(String[] args) {
		Fish f = new Fish("쿠피");
		f.print();
		f.breath();
	}
	
}
