package exam.spring.board.dao;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import exam.spring.board.config.ApplicationConfig;
import exam.spring.board.dto.Member;

@RunWith(SpringJUnit4ClassRunner.class)  //요 클래스가 실행될때 spirngjunit4classrunner가 도와줌)
@ContextConfiguration(classes = {ApplicationConfig.class})
@Transactional //얘가 @test에서 실행시키면 롤백을 시켜서 실제로 디비에는 저장되지않음  
public class MemerbersMapperTest {
	@Autowired
	MembersMapper membersMapper;
	
	@Test
	public void getUser() throws Exception{
		Member member = membersMapper.getMember("ko");
		Assert.assertNotNull(member);
		System.out.println("hohoho");
		Assert.assertEquals("a", member.getName());
	}
	
	@Test
	public void addMember()throws Exception {
		Member testMember = new Member();
		testMember.setId("st");
		testMember.setName("test");
		testMember.setPassword("1234");
		testMember.setEmail("ttest@test.com");
		
		membersMapper.addMember(testMember);
		
		Assert.assertNotNull(membersMapper.getMember("ttest"));
	}
	
	@Test
	public void getMembers()throws Exception {
		List<Member> memberList = membersMapper.getMembers();
		Assert.assertEquals(7, memberList.size());
	}
}
