package exam.spring.board.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import exam.spring.board.dto.Member;
// 스프링에서는 dao를 인터페이스로 정의하고 @Mapper를 이용하여 매퍼를 통해 디비와 정보를 주고받음 
@Mapper
public interface MembersMapper {
	public Member getMember(String id);
	public int addMember(Member member);
	public List<Member> getMembers();
	public void updateMember(Member member);
	public int deleteMember(String id);
}