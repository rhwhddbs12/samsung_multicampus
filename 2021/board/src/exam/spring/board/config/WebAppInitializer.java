package exam.spring.board.config;

import javax.servlet.Filter;

import org.springframework.web.filter.CharacterEncodingFilter;
import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;
//*********** DispatcherServlet 생성(설정)****************
//AbstractAnnotationConfigDispatcherServletInitializer를 상속받은 클래스로 
//DispatcherServlet과 애플리케이션의 서블릿 컨텍스트내의 스프링 애플리케이션 컨텍스트를 설정한다는 의미이다.

//***내부적으로 DispatcherServlet과 ContextLoaderListener를 생성

//* DispatcherServlet은 컨트롤러, 뷰 리졸버, 핸들러 매핑과 같은 웹 컴포넌트가 포함된 빈을 로딩하며, 
//* ContextLoaderListener는 애플리케이션 내의 그 외의 다른 빈을 로딩 (MyBatis,DataSource DB연동)
public class WebAppInitializer extends AbstractAnnotationConfigDispatcherServletInitializer{

	
	//	ContextLoaderListener가 생성한 \
	
//	반면 DispatcherServlet의 정의는 WebConfig에 선언되어 있다.
	@Override
	protected Class<?>[] getRootConfigClasses() {
		
		return new Class<?>[] {ApplicationConfig.class}; //->MyBatis ,DB연동 
	}

	@Override
//DispatcherServlet이 애플리케이션 컨텍스트를 WebConfig 설정 클래스(java 설정)에서 (여기서는 MvcConfig)
//정의된 빈으로 로딩하도록 되어있다. 
	//protected는 동일 클래스 또는 해당 클래스를 상속받은 외부 패키지의 클래스에서 접근 가능 .
	protected Class<?>[] getServletConfigClasses() {
		return new Class<?>[] {MvcConfig.class}; //-> 컨트롤러,뷰 리졸버, 핸들러 매핑 
	}
	//DispatcherServlet이 매핑되기 위한 하나 혹은 여러 개의 패스를 지정한다.
	@Override
	protected String[] getServletMappings() {
		return new String[] {"/"};
	}
	//스프링에서 제공해주는 필터는 web.xml에서 사용하는데 자바는 여기서 해주면 된다. 
	@Override
	protected Filter[] getServletFilters() {
		CharacterEncodingFilter encodingFilter = new CharacterEncodingFilter();
		encodingFilter.setEncoding("utf-8");
		
		return new Filter[] {encodingFilter};
	}

	
}
